<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class GoodDeal extends Model
{
    protected $table = "gooddeal";

    protected $fillable = ['id','name',  'description', 'startDate','endDate', 'quantity', 'discount','price','image', 'Product_id'];


    public function typeProduct(): HasOne
    {
        return $this->hasOne('App\typeProduct', 'id', 'typeProduct_id');
    }


}
