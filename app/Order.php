<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';
    protected $fillable =['id','state','deliveryAddress','city','zipCode','priceTTC','priceHTC','deliveryDate','timeSlots','hoursClick&Collect','sendDate','shop_id','user_id','deliveryMode'];
    public $timestamps = true;

    public static function addDeliveryAddress($user_id,$deliveryAddress, $city, $zipCode,$deliveryMode)
    {
        Order::where('user_id', $user_id)->where('state', '=', 'UserAdding')->update(array('deliveryAddress' => $deliveryAddress,'city' => $city,'zipCode' => $zipCode,'deliveryMode'=>$deliveryMode));
        return true;
    }

}
