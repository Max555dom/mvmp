<?php

namespace App\Providers;

use Darryldecode\Cart\Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        View::composer(['layouts.app', 'productPage'], function ($view) {
            $view->with([
                'cartCount' => \Cart::getTotalQuantity(),
                'cartTotal' => \Cart::getTotal(),
            ]);
        });
    }
}
