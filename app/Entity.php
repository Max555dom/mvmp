<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Entity extends Authenticatable
{
    protected $table='entity';
    protected $fillable =['id','city','zip_code','address','email','password','numero'];
    public $timestamps = true;

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getAuthPassword()
    {
        return $this->password;
    }
}
