<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListProductOrder extends Model
{
    //list_product_order
    protected $table='list_product_order';
    protected $fillable =['id','product_id','order_id'];

    public function productOrder()
    {
        return $this->hasMany('App\Order', 'id', 'order_id');
    }


}
