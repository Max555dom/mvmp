<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class StoreInscriptionCommerceRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'siret' => 'required|min:14|unique:shop,siret',
            'siren' => 'required|min:9|unique:shop,siren',
            'image' => 'required|image',
            'clickandcollect' => 'required',
            'deliveryHome' => 'required',
            'deliveryPoint' => 'required',
            'state' => 'required',
            'typeShop_id' => 'required',
            'entity_id' => 'required|unique:shop,entity_id',
            'city' => 'required',
            'zip_code' => 'required',
            'address' => 'required',
            'numero' => 'required',
            'document' =>'required'
        ];
    }
}
