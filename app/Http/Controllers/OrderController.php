<?php

namespace App\Http\Controllers;

use App\ListProductOrder;
use App\Shop;
use Illuminate\Support\Facades\DB;
use App\Order;
use Hamcrest\Type\IsInteger;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index($id)
    {
        //Récup toute les orders de l'utilisateur
        $resultsOrder = DB::table('order')
            ->select(DB::raw('*'))
            ->orderBy('id', 'ASC')
            ->where('user_id', $id)
            ->where('state', '!=', 'UserAdding')
            ->get();

        //Init d'un tableau vide où l'on va voir mettre toute les commandes et un index pour naviger dedans
        $order = [];
        $idx = 0;
        $totalpriceTTCByOrder = 0;
        // discount ini
        $discount = 0;

        foreach ($resultsOrder as $oneOrder) {

            //récupération de l'id de la commande pour le changement de son état
            $order[$idx]['order_id'] = $oneOrder->id;

            //récup le nom du shop
            $order[$idx]['shop_name'] = DB::table('shop')
                ->select(DB::raw('name'))
                ->where('shop.id', '=', $oneOrder->shop_id)
                ->pluck('name');

            //récup la date de livraison de la commande
            $order[$idx]['order_delivery'] = $oneOrder->deliveryDate;

            //récup state
            $order[$idx]['order_state'] = $oneOrder->state;

            //récup tout les produits qu'il y a dans une commande
            $order[$idx]['product'] = DB::table('product')
                ->select('product.name', 'product.priceTTC', 'product.priceHT', 'product.image')
                ->join('list_product_order', 'product.id', '=', 'list_product_order.product_id')
                ->join('order', 'order.id', '=', 'list_product_order.order_id')
                ->where('order.id', '=', $oneOrder->id)
                ->get();

            foreach ($order[$idx]['product'] as $product) {
                $totalpriceTTCByOrder = $totalpriceTTCByOrder + $product->priceTTC;
            }
            // discount added
            $discount +=   $this->getDiscount($oneOrder->shop_id, $totalpriceTTCByOrder);
            $order[$idx]['order_price'] = $totalpriceTTCByOrder;

            $totalpriceTTCByOrder = 0;

            $idx = $idx + 1;
        }


        return view(
            'order',
            [
                '$resultsOrder' => $resultsOrder,
                'order' => $order
            ]
        );
    }

    public function storeOrder()
    {

        $content = \Cart::getContent();


        //Ajoute des produit du panier a la table Order et ListProductOrder

        foreach ($content as $item) {
            if ($item->quantity > 1) {

                for ($i = 0; $i < $item->quantity; $i++) {
                    if (DB::table('order')->where('shop_id', $item->shop_id)->where('state', 'UserAdding')->where('user_id', 1)->count() >= 1) {

                        $order = DB::table('order')->where('shop_id', $item->shop_id)
                            ->where('state', 'UserAdding')
                            ->where('user_id', '1')
                            ->first();

                        ListProductOrder::Create([
                            'product_id' => $item->id,
                            'order_id' => $order->id,
                        ]);
                    } else {
                        $order = Order::Create([
                            'state' => 'UserAdding',
                            'shop_id' => $item->shop_id,
                            'user_id' => 1,
                        ]);
                        ListProductOrder::Create([
                            'product_id' => $item->id,
                            'order_id' => $order->id,
                        ]);
                    }
                }
            } else {
                if (DB::table('order')->where('shop_id', $item->shop_id)->where('state', 'UserAdding')->where('user_id', 1)->count() >= 1) {
                    $order = DB::table('order')
                        ->where('shop_id', $item->shop_id)
                        ->where('state', 'UserAdding')
                        ->where('user_id', 1)
                        ->first();

                    ListProductOrder::Create([
                        'product_id' => $item->id,
                        'order_id' => $order->id,

                    ]);
                } else {
                    $order = Order::Create([
                        'state' => 'UserAdding',
                        'shop_id' => $item->shop_id,
                        'user_id' => 1,
                    ]);
                    ListProductOrder::Create([
                        'product_id' => $item->id,
                        'order_id' => $order->id,

                    ]);
                }
            }
        }

        //Effacement du panier
        foreach ($content as $item) {
            \Cart::remove($item->id);
        }


        return view('ChooseDeliveryMode');
    }


    public function DeliveryMode()
    {
        DB::table('order')->where('state', 'UserAdding')->where('user_id', 1)->update(['deliveryMode' => 2]);
        return view('formClassicDelivery');
    }


    public function getPaiment()
    {
        //Get the id of the connected user
        $user_id = 1;
        //Get orderById
        $resultsOrder = DB::table('order')
            ->select(DB::raw('*'))
            ->where('order.user_id', 1)
            ->where('state', '=', 'UserAdding')
            ->orderBy('id', 'ASC')
            ->get();

        //Init d'un tableau vide où l'on va voir mettre toute les commandes et un index pour naviger dedans
        $order = [];
        $idx = 0;
        $totalpriceHT = 0;
        $totalpriceTTC = 0;
        $totalpriceHTByOrder = 0;
        $totalpriceTTCByOrder = 0;

        //création du tableau
        $totalDiscount = 0;
        foreach ($resultsOrder as $oneOrder) {

            //récupération de l'id de la commande pour le changement de son état
            $order[$idx]['id'] = $oneOrder->id;

            //récup le nom du shop
            $order[$idx]['shop_name'] = DB::table('shop')
                ->select(DB::raw('name'))
                ->where('shop.id', '=', $oneOrder->shop_id)
                ->pluck('name');

            //récup la date de livraison de la commande
            $order[$idx]['order_delivery'] = $oneOrder->deliveryDate;

            //récup tout les produits qu'il y a dans une commande
            $order[$idx]['product'] = DB::table('product')
                ->select('product.name', 'product.priceTTC', 'product.priceHT', 'product.image')
                ->join('list_product_order', 'product.id', '=', 'list_product_order.product_id')
                ->join('order', 'order.id', '=', 'list_product_order.order_id')
                ->where('order.id', '=', $oneOrder->id)
                ->get();

            foreach ($order[$idx]['product'] as $product) {
                $totalpriceTTCByOrder = $totalpriceTTCByOrder + $product->priceTTC;
                $totalpriceHTByOrder = $totalpriceHTByOrder + $product->priceHT;
                $totalpriceHT = $totalpriceHT + $product->priceHT;
                $totalpriceTTC = $totalpriceTTC + $product->priceTTC;
            }
            $discount =   $this->getDiscount($oneOrder->shop_id, $totalpriceTTCByOrder);
            $totalDiscount +=  $discount;
            $order[$idx]['order_price'] = $totalpriceTTCByOrder - $discount;
            $order[$idx]['order_priceHT'] = $totalpriceHTByOrder - $discount;

            $totalpriceTTCByOrder = 0;
            $totalpriceHTByOrder = 0;
            $idx = $idx + 1;
        }


        return view(
            'Paiment',
            [
                'order' => $order,
                'resultsOrder' => $resultsOrder,
                'totalpriceHT' => $totalpriceHT - $totalDiscount,
                'totalpriceTTC' => $totalpriceTTC  - $totalDiscount,
                'discount' => $totalDiscount
            ]
        );
    }




    /**
     * check if the user has joined the loyalty program (got by idShop)
     */
    public function checkAdhesion($id)
    {

        $loyaltyClient =  $this->getFidelity($id);
        if ($loyaltyClient->id > 0) : return true;
        else : return false;
        endif;
    }



    /**
     * get fidelity infos about the user for the selected program
     */
    public function getFidelity($id)
    {

        // user_id = Auth::user()->id
        $user_id = 1;
        $fidelity = DB::table('fidelity')
            ->select(DB::raw('*'))
            ->where('fidelity.program_id', $id)
            ->where('fidelity.user_id', $user_id)
            ->get();

        return $fidelity;
    }

    /**
     * get loyalty program of shop 
     */
    public function getProgram($id)
    {

        $loyaltyProgram      = "loyaltyProgram";
        $loyaltyProgram = '"' . $loyaltyProgram . '"';

        $minPriceByCommande = "minPriceByCommande";
        $minPriceByCommande = '"' . $minPriceByCommande . '"';

        $pointCommande = "pointCommande";
        $pointCommande = '"' . $pointCommande . '"';

        $program = DB::table('shop')
            ->select(DB::raw('' . $loyaltyProgram . '.id as program_id, shop.name as name, ' . $loyaltyProgram . '.shop_id as shop_id,  ' . $loyaltyProgram . '.image as image, ' . $loyaltyProgram . '.description as description, ' . $minPriceByCommande . ' , ' . $pointCommande . ' '))
            ->join('loyaltyProgram', 'loyaltyProgram.shop_id', '=', 'shop.id')
            ->where('shop_id', $id)
            ->first();

        return $program;
    }

    /**
     * get targets points for the selected loyalty program 
     */
    public function getTargetPoints($id)
    {
        $targets =  DB::table('pallierProgram')
            ->select(DB::raw('*'))
            ->where('loyalty_program_id', $id)
            ->orderBy('objectifPoint')
            ->get();

        return $targets;
    }

    /**
     * Calculating the point of user regarding the 
     */
    public function getDiscount($id, $value)
    {
        // get the program id from id shop and fidelity infos
        $program_id = $this->getProgram($id)->program_id;
        $minPriceByCommande = $this->getProgram($id)->minPriceByCommande;
        $fidelity = $this->getFidelity($program_id);
        $convertedBonus = 0;
        // 1 : check if the user has joined the program of the shop for each order
        if (count($fidelity) == 1) :
            // if yes
            // 2 : get total amount of the order
            $totalAmount = $value;
            // 3 : get the targets of points of the selected loyalty program
            $targetPoints = $this->getTargetPoints($program_id);
            foreach ($targetPoints as $target) :
                // 4 : compare the total amount with the min price of the curent target point
               // if ($totalAmount >= $minPriceByCommande) :
                    // 5 : compare if the lastpointtarget is not equal to the curent 
                    if ($fidelity[0]->lastPointTarget != $target->objectifPoint) :
                        // 6 compare userPoint with targetPoint
                        if ($fidelity[0]->pointUser >= $target->objectifPoint) :
                            // 7 : convert bonus of level of loyalty program to int 
                            $bonus = $target->bonus;
                            $convertedBonus = "";
                            for ($i = 0; $i < strlen($bonus); $i++) :
                                if (is_numeric($bonus[$i]) || is_string($bonus[$i])) :
                                    if (is_numeric($bonus[$i])) :
                                        $convertedBonus .=  '' . $bonus[$i];
                                    else :
                                        // 
                                        if ($bonus[$i] == '.' || $bonus[$i] == '%' || $bonus[$i] == '€' || $bonus[$i] == '$') :
                                            if ($bonus[$i] == '%') :
                                                $convertedBonus =   ($convertedBonus / 100) * $totalAmount;
                                                $convertedBonus = floatval($convertedBonus);
                                                return $convertedBonus;
                                            elseif ($bonus[$i] == '€' || $bonus[$i] == '') :
                                                $convertedBonus .=  '' . intval($bonus[$i]);
                                                return  $convertedBonus;
                                            else :
                                                return  $convertedBonus =  0;
                                            endif;
                                        endif;
                                    endif;
                                endif;
                            endfor;
                        else :
                            break;
                        endif;
                    endif;
              //  else :
                    // dd($totalAmount);
                  //  return 0;
              //  endif;
            endforeach;
            // 8 : return bonus
            if ($convertedBonus != "") :
                return $convertedBonus;
            else :
                return 0;
            endif;
        // if not
        else :
            // return 1
            return 0;
        endif;
    }
}
