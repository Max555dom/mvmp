<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class mapCommercesController extends Controller
{
    public function index()
    {
        $Idtype      = "typeShop_id";
        $Types = DB::table('shop')
            ->select(DB::raw('type.name as name,type.id as id'))
            ->join('type', 'type.id', '=', 'shop.typeShop_id')
            ->join('entity', 'entity.id', '=', 'shop.entity_id')
            ->where('state', 'Validate')
            ->distinct()
            ->get();

        // prepare columns options for datatable
        $aColumnsOptions = [
            [
                'title'      => 'Id',
                'name'       => 'id',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => false
            ],
            [
                'title'      => 'Image',
                'name'       => 'image',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true,

            ],
            [
                'title'      => 'Nom',
                'name'       => 'name',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Description',
                'name'       => 'description',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true
            ],
            [
                'title'      => 'Statut',
                'name'       => 'state',
                'type'       => 'hidden',
                'searchable' => false,
                'visible'    => false
            ],
            [
                'title'      => 'Type',
                'name'       => '"' . $Idtype . '"',
                'type'       => 'hidden', // voir pour mettre en hidden la colonne 
                'searchable' => true,
                'visible'    => false
            ],
            [
                'name'       => 'button',
                'type'       => 'button',
                'visible'    => true,
                'targets' => 7,
                'data' => null,
                'defaultContent' => ""
            ],
        ];

        return view('map')
            ->with('Columns', $aColumnsOptions)
            ->with('resultsType', $Types);
    }


    /**
     * get Markers for google map in xml format
     */
    public function getDataXml()
    {
        // sql request
        $zipCode      = "zipCode";
        $zipCodeFormated = '"' . $zipCode . '"';
        $shops = DB::table('shop')
            ->select(DB::raw('shop.id, shop.name, shop.description, shop.image, shop.grade, entity.address, entity.' . $zipCodeFormated . ', entity.city'))
            ->join('entity', 'entity.id', '=', 'shop.entity_id')
            ->where('shop.state', 'Validate')
            ->get();

        return response()->view('markers', compact('shops'))->header('Content-Type', 'text/xml');
    }

    /**
     * get Address of shops in json format
     */
    public function getShopsAddress()
    {
        // sql request
        $zipCode      = "zipCode";
        $zipCodeFormated = '"' . $zipCode . '"';
        $shops = DB::table('shop')
            ->select(DB::raw(' entity.address, entity.' . $zipCodeFormated . ', entity.city'))
            ->join('entity', 'entity.id', '=', 'shop.entity_id')
            ->where('shop.state', 'Validate')
            ->get();
        return view('map')
            ->with('shops', $shops);
    }
}
