<?php

namespace App\Http\Controllers;

use App\Order;
use App\ListProductOrder;
use App\Product;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ShoppingBasketController extends Controller
{
    public function detail($id)
    {
        $orders = order::Where('user_id', $id)->where('state', 'UserAdding')->get();

        foreach ($orders as $oneOrder){
        $shops = Shop::Where('id', $oneOrder->shop_id)->get();

        }

        return view('shoppingBasket')->with('orders', $orders)->with('shops',$shops);
    }

    public function store( Product $product, $user_id )
    {

        Order::create([
            'state'=>'UserAdding',
            'priceTTC'=>0,
            'priceHTC'=>0,
            'shop_id'=>$product->shop_id,
            'user_id'=>$user_id
        ]);
        $order = Order::where('user_id',$user_id)->where('shop_id',$product->shop_id)->get();
        ListProductOrder::create([
            'id_product'=>$product->id,
            'id_order'=>$order->id

        ]);
        return back();

    }

    public function update(Product $product, $user_id)
    {
        //if order create or not

        $order = Order::where('user_id',$user_id)->where('shop_id',$product->shop_id)->get();

        if ($order =! NULL){
            ListProductOrder::create([
                'id_product'=>$product->id,
                'id_order'=>$order->id

            ]);
        }
        else{
            route('storeBasket',$product,$user_id);
        }

    }


}

