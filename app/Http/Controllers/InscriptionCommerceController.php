<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Http\Requests\StoreInscriptionCommerceRequests;
use App\Shop;
use App\Type;
use App\Entity;
use Illuminate\Support\Facades\DB;


class InscriptionCommerceController extends Controller
{
    public function index()
    {
        #recherche en base de tableau de type de commerce
        $shop = null;
        $typeShops = Type::all();
        return view('inscriptionCommerce')
            ->with('shop',$shop)
            ->with('typeShops', $typeShops);
    }

    public function inscriptionCommerce(StoreInscriptionCommerceRequests $request)
    {
        //TODO faire les messages d'erreur -> le siret le siren existe dejà ou vous avez dejà créer un commerce
        $name = 'shopImage.$idShop';
        $params = $request;
        Shop::Create([
            'name' => $params['name'],
            'siret' => $params['siret'],
            'siren' => $params['siren'],
            'image' => $name,
            'click&collect' => $params['clickandcollect'],
            'deliveryHome' => $params['deliveryHome'],
            'deliveryPoint' => $params['deliveryPoint'],
            'state' => $params['state'],
            'typeShop_id' => $params['typeShop_id'],
            'entity_id' => $params['entity_id'],
        ]);

        Entity::where('id', $params['entity_id'])
            ->update(array('city' => $params['city'], 'numero' => $params['numero'], 'zipCode' => $params['zip_code'], 'address' => $params['address']));

        //Script d'enrigtrement de l'image
        $idShop = DB::table('shop')
                ->select(DB::raw('id'))
                ->where('name', $params['name'])
                ->get()[0]->id;

        $image_content = $_FILES['image']['tmp_name'];
        move_uploaded_file($image_content, "../storage/app/public/ImagesShop/shopImage".$idShop.".png" );

        $file_content = $_FILES['document']['tmp_name'];
        move_uploaded_file($file_content, "../storage/app/public/Documents/documentShop".$idShop.".pdf" );

        Shop::where('id', $params['entity_id'])
            ->update(array('image' => "shopImage".$idShop.".png"));

        Attachment::Create([
            'doc' => "documentShop".$idShop.".pdf",
            'shop_id' => $idShop,
        ]);

        //TODO quand page d'accueil -> rediriger vers page d'accueil puis mettre secu profil si shop valider
        return redirect(route('profilCommerce',$idShop));
    }

   public function waitingValue($id){
       $shop = Shop::Where('id', $id)->firstOrFail();
       $typeShops = Type::all();
       $typeShopId = $shop->typeShop_id;
       $type= Type::Where('id',$typeShopId)->firstOrFail();
       $entity_id = $shop->entity_id;
       $entity = Entity::Where('id', $entity_id)->firstOrFail();

       return view('inscriptionCommerce')
           ->with('shop',$shop)
           ->with('type',$type)
           ->with('typeShops',$typeShops)
           ->with('entity',$entity);
   }


}
