<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AccueilCommercesController extends Controller
{
    public function index()
    {
        $Idtype      = "typeShop_id";
        $Types = DB::table('shop')
            ->select(DB::raw('type.name as name,type.id as id'))
            ->join('type', 'type.id', '=', 'shop.typeShop_id')
            ->where('state', 'Validate')
            ->distinct()
            ->get();

        // prepare columns options for datatable
        $aColumnsOptions = [
            [
                'title'      => 'Id',
                'name'       => 'id',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => false
            ],
            [
                'title'      => 'Image',
                'name'       => 'image',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true,

            ],
            [
                'title'      => 'Nom',
                'name'       => 'name',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Description',
                'name'       => 'description',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true
            ],
            [
                'title'      => 'Statut',
                'name'       => 'state',
                'type'       => 'hidden',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Type',
                'name'       => '"' . $Idtype . '"',
                'type'       => 'hidden', // voire pour mettre en hidden la colonne
                'searchable' => true,
                'visible'    => false
            ],
            [
                'name'       => 'button',
                'type'       => 'button', // voire pour mettre en hidden la colonne
                'targets' => -1,
                'data' => null,
                'defaultContent' => "<label class='btn btn-success boutique'>
                                    Accès a la boutique
                                </label> "
            ]
        ];

        return view('AccueilCommerces')
            ->with('Columns', $aColumnsOptions)
            ->with('resultsType', $Types);
    }

    public function getShop(Request $request, $id_Shop = -1)
    {
        $FBlink      = "FBlink";
        $FBlink = '"' . $FBlink . '"';
        $Instalink      = "Instalink";
        $Instalink = '"' . $Instalink . '"';
        $YTlink      = "YTlink";
        $YTlink = '"' . $YTlink . '"';


        $Shop = DB::table('shop')
            ->select(DB::raw('shop.id,shop.name,shop.description,shop.image,shop.' . $FBlink . ',shop.' . $Instalink . ',shop.' . $YTlink . ',shop.grade,shop.intro'))
            ->join('type', 'type.id', '=', 'shop.typeShop_id')
            ->where('state', 'Validate')
            ->where('shop.id', $id_Shop)
            ->get();

        $dataShop  = array();
        foreach ($Shop as $oneVal) {
            $temp = (array)$oneVal;
            array_push($dataShop, $temp);
        }


        $Idtype      = "typeProduct";
        $var = '"' . $Idtype . '"';
        $Types = DB::table('product')
            ->select(DB::raw($var . '.name as name,' . $var . '.id as id'))
            ->join($Idtype, $Idtype . '.id', '=', 'product.' . $Idtype . '_id')
            ->where('shop_id', $id_Shop)
            ->distinct()
            ->get();



        $price      = "priceTTC";
        $type      = "typeProduct_id";
        // prepare columns options for datatable
        $aColumnsOptions = [
            [
                'title'      => 'Id',
                'name'       => 'id',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => false
            ],
            [
                'title'      => 'Image',
                'name'       => 'image',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => false,

            ],
            [
                'title'      => 'Nom',
                'name'       => 'name',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Prix',
                'name'       => '"' . $price . '"',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => true
            ],
            [
                'title'      => 'Type',
                'name'       => '"' . $type . '"',
                'type'       => 'hidden', // voire pour mettre en hidden la colonne
                'searchable' => true,
                'visible'    => false
            ],
        ];


        return view('Commerce')
            ->with('Columns', $aColumnsOptions)
            ->with('Shop', $dataShop)
            ->with('resultsType', $Types)
            ->with('report' , $this->checkReport($id_Shop));
    }

    public function commerceValidation(Request $request, $id_Shop = -1){ # deplacer dans chop controller

        #shop, attachment, typeShop, entity
        # par default = 0 = "Waiting"
        # activate = state = Validate
        #disabled = desactivation manuel
        #attachment

        $zipCode      = "zipCode";
        $zipCode = '"'.$zipCode.'"';
        $deliveryPoint      = "deliveryPoint";
        $deliveryPoint = '"'.$deliveryPoint.'"';
        $deliveryHome      = "deliveryHome";
        $deliveryHome = '"'.$deliveryHome.'"';
        $clickcollect      = "click&collect";
        $clickcollect = '"'.$clickcollect.'"';
        $commerce = DB::table('attachment')
                    ->select(DB::raw("shop.id, shop.name as shopName, entity.address, entity.".$zipCode.", entity.numero, type.name,shop.siret, shop.siren, entity.email, attachment.doc, shop.image, shop.".$clickcollect." as clickcollect, shop.".$deliveryPoint.", shop.".$deliveryHome))
                    ->join("shop",'shop.id','=','attachment.shop_id')
                    ->join("type",'type.id','=','shop.typeShop_id')
                    ->join("entity",'entity.id','=','shop.entity_id')
                    ->where("attachment.shop_id","=",$id_Shop)
                    ->get();








        return view('ValidationCommerce')->with('commerce',$commerce[0]);
    }


    public function programeFidelity(Request $request, $id_Shop = -1)
    {
        $programme = DB::table('loyaltyProgram')
        ->select(DB::raw('*'))
        ->where('shop_id', $id_Shop)
        ->get();
        if(!isset($programme[0]))
        {
            return view('ProgrameFidelity')
                    ->with('id_Shop', $id_Shop);
        }else{


            $palliers = DB::table('pallierProgram')
                        ->select(DB::raw('*'))
                        ->where('loyalty_program_id', $programme[0]->id)
                        ->orderBy('objectifPoint')
                        ->get();

            return view('ViewProgrameFidelity')
                    ->with('palliers', $palliers)
                    ->with('programme', $programme);
        }
    }

     public function getDownload(Request $request, $document_name){ # deplacer dans chop
        //PDF file is stored under storage folder linked with the public folder
        // https://laravel.com/docs/8.x/filesystem for more information

        $file_path = storage_path('app/public/Documents/'.$document_name.".pdf");
        return response()->download($file_path);
    }

    public function commerceGetStock(Request $request, $id_Shop = -1){

        $Shop = DB::table('shop')
                            ->select(DB::raw('shop.id,shop.name'))
                            ->join('type','type.id','=','shop.typeShop_id')
                            ->where('shop.id', $id_Shop)
                            ->get();

        $dataShop  = array();
        foreach($Shop as $oneVal) {
            $temp = (array)$oneVal;
            array_push($dataShop, $temp);
        }


        $typeProduct      = "typeProduct";
        $type = '"'.$typeProduct.'"';
        $Types = DB::table('product')
                            ->select(DB::raw($type.'.name as name,'.$type.'.id as id' ))
                            ->join($typeProduct,$typeProduct.'.id','=','product.'.$typeProduct.'_id')
                            ->where('shop_id', $id_Shop)
                            ->distinct()
                            ->get();



        $priceTTC      = "priceTTC";
        $priceHT      = "priceHT";
        $type      = "typeProduct_id";
         // prepare columns options for datatable
         $aColumnsOptions = [
        [
            'title'      => 'Id',
            'name'       => 'id',
            'type'       => 'text',
            'searchable' => false,
            'visible'    => false
        ],
        [
            'title'      => 'Nom',
            'name'       => 'name',
            'type'       => 'text',
            'searchable' => true,
            'visible'    => true
        ],
        [
            'title'      => 'Stock',
            'name'       => 'inventory',
            'type'       => 'text',
            'searchable' => true,
            'visible'    => true
        ],
        [
            'title'      => 'Prix HT',
            'name'       => '"'.$priceHT.'"',
            'type'       => 'text',
            'searchable' => true,
            'visible'    => true
        ],
        [
            'title'      => 'Prix TTC',
            'name'       => '"'.$priceTTC.'"',
            'type'       => 'text',
            'searchable' => true,
            'visible'    => true
        ],
        [
            'title'      => 'Reference',
            'name'       => 'reference',
            'type'       => 'text',
            'searchable' => true,
            'visible'    => true
        ],
        [
            'title'      => 'Type',
            'name'       => '"'.$type.'"',
            'type'       => 'text', // voire pour mettre en hidden la colonne
            'searchable' => true,
            'visible'    => false
        ],
    ];

         //dd($aColumnsOptions);
        return view('CommerceStock')
            ->with('Columns', $aColumnsOptions)
            ->with('Shop',$dataShop)
            ->with('resultsType',$Types);
    }

    public function commerceStockCreate(Request $request, $id_Shop = -1){
        return view('CreateStock')
        ->with('id_Shop', $id_Shop);
    }

    public function commerceStockUpdate(Request $request, $id_product = -1){

        $priceTTC     = "priceTTC";
        $CarnelpriceTTC = '"'.$priceTTC.'"';

        $priceHT      = "priceHT";
        $CarnelpriceHT = '"'.$priceHT.'"';

        $typeProduct      = "typeProduct";
        $type = '"'.$typeProduct.'"';
        $product = DB::table('product')
                            ->select(DB::raw('product.id ,product.name as name, product.image, '.$type.'.name as typename, product.reference, product.description, product.'.$CarnelpriceTTC .', product.'.$CarnelpriceHT.', brand, inventory, shop_id' ))
                            ->join($typeProduct,$typeProduct.'.id','=','product.'.$typeProduct.'_id')
                            ->where('product.id','=',$id_product)
                            ->get();

        if(isset($product[0])){
            return view('UpdateStock')
                ->with('product', $product[0]);
        }
    }

  /**
   * Checking if the shop has been reported by any user.
   */
  public function checkReport($id){
    $id = htmlspecialchars($id);
    $shop = Report::where('shop_id', $id)->get();
    if(count($shop)<=0):
        return true;
    else:
        return false;
    endif;
}

}
