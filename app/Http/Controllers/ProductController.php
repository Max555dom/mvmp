<?php

namespace App\Http\Controllers;

use App\Product;
use App\Review;

use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        //Return view with all product
        return view('productPage')->with('products', $products);
    }

    /**
     * Details of product
     * @param $id
     * @return Factory|View
     */
    public function details($id)
    {
        $product = Product::Where('id', $id)->firstOrFail();
        $reviews = review::where('product_id', $id)->get();
        //$users = User::where('id', $reviews->user_id)->get();
        $products = Product::where('typeProduct_id', $product->typeProduct_id)->get();
        $isGooddeal = false;
        return view('productPage')
            ->with('product', $product)
            ->with('reviews', $reviews)
            ->with('isGooddeal', $isGooddeal)
            ->with('products', $products);

    }

    /**
     * List of products per Shop
     */
    public function listProductsShop()
    {

        $Idtype      = "typeProduct_id";
        $table = "typeProduct";
        $var = '"' . $Idtype . '"';
        $var2 = '"' . $table . '"';
        $Types = DB::table('product')
            ->select(DB::raw($var2 . '.name as name,' . $var2 . '.id as id'))
            ->join($table, $table . '.id', '=', 'product.' . $table . '_id')
            ->distinct()
            ->get();


        // prepare columns options for datatable
        $aColumnsOptions = [
            [
                'title'      => 'Id',
                'name'       => 'id',
                'type'       => 'text',
                'searchable' => false,
                'visible'    => false
            ],
            [
                'title'      => 'Image',
                'name'       => 'image',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true,

            ],
            [
                'title'      => 'Nom',
                'name'       => 'name',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'SousNom',
                'name'       => 'subname',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Description',
                'name'       => 'description',
                'type'       => 'text',
                'searchable' => true,
                'visible'    => true
            ],
            [
                'title'      => 'Statut',
                'name'       => 'state',
                'type'       => 'hidden',
                'searchable' => true,
                'visible'    => false
            ],
            [
                'title'      => 'Type',
                'name'       => '"' . $Idtype . '"',
                'type'       => 'hidden', // voir pour mettre en hidden la colonne 
                'searchable' => true,
                'visible'    => false
            ],
            [
                'name'       => 'button',
                'type'       => 'button', // voir pour mettre en hidden la colonne 
                'targets' => -1,
                'data' => null,
                'defaultContent' => "<label class='btn btn-success produit'>
                                    Voir l'article
                                </label> "
            ]
        ];

        return view('Products')
            ->with('Columns', $aColumnsOptions)
            ->with('resultsType', $Types);
    }
}
