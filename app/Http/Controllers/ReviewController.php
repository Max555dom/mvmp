<?php

namespace App\Http\Controllers;

use App\review;
use App\Http\Requests\StoreReviewRequests;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Nullable;

class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::all();
        return view('ListReviews')->with('reviews', $reviews);
    }
    public function details($id)
    {
        $review = Review::findOrFail($id);
        return view('detailsReview')->with('review', $review);
    }
    public function store(StoreReviewRequests $request)
    {
        $params = $request->validated();
        review::Create([
            'user_id'=>$params['user_id'],
            'shop_id'=>$params['shop_id'],
            'product_id'=>$params['product_id'],
            'title'=>$params['title'],
            'description'=>$params['description'],
            'grade'=>$params['grade'],



           ]);
        return redirect()->route('detailProduct',$params['product_id']);
    }




}
