<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateCommerceRequests;
use App\Type;
use App\Shop;
use App\Entity;


class ProfilCommerceController extends Controller
{
    public function index($id)
    {
        $shop = Shop::Where('id', $id)->firstOrFail();
        $typeShopId = $shop->typeShop_id;
        $typeShop = Type::Where('id',$typeShopId)->firstOrFail();
        $entity_id = $shop->entity_id;
        $entity = Entity::Where('id', $entity_id)->firstOrFail();

        return view('profilCommerce')
            ->with('shop',$shop)
            ->with('typeShop',$typeShop)
            ->with('entity',$entity);

    }

    public function updateProfilCommerce(UpdateCommerceRequests $request){
        {
            $params = $request;
            $id= $params['id'];
            Shop::where('id', $params['id'])
            ->update([
                'intro' => $params['intro'],
                'description' =>$params['description'],
                'hours' => $params['hours'],
                'logo' => "shopLogo$id.png",
                'click&collect' => $params['clickandcollect'],
                'deliveryHome' => $params['deliveryHome'],
                'deliveryPoint' => $params['deliveryPoint'],
                'FBlink' => $params['FBlink'],
                'Instalink' => $params['Instalink'],
                'YTlink' => $params['YTlink'],
            ]);

            Entity::where('id', $params['entity_id'])
                ->update(array('mail' => $params['mail'], 'numero' => $params['numero']));

            $image_content = $_FILES['logo']['tmp_name'];
            move_uploaded_file($image_content, "../storage/app/public/ImagesShop/shopLogo".$id.".png" );

            return redirect(route('profilCommerce',$id));
        }
    }
}
