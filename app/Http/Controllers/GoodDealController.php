<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\GoodDeal;
use App\Review;

class GoodDealController extends Controller
{
    //Liste des bon plans
    public function index()
    {
        $Idtype = "typeProduct_id";
        $table = "typeProduct";
        $productid = "Product_id";
        $shopname ="shopName";
        $startdate = "startDate";
        $enddate = "endDate";

        $var2 = '"' . $table . '"';

        $productid = "Product_id";
        $startdate = "startDate";
        $enddate = "endDate";
        $Idtype = "typeProduct_id";
        $typeproduct ="typeProduct";
        $carnelProductId = '"' . $productid . '"';
        $carnelStartDate = '"' . $startdate . '"';
        $carnelEndDate = '"' . $enddate . '"';
        $carnelType = '"' . $Idtype . '"';
        $carnelTypeProduct = '"' . $typeproduct . '"';
        $status       = "'Validate'";
        //Type de produit
        $Types = DB::table('product')
            ->select(DB::raw($var2 . '.name as name,' . $var2 . '.id as id'))
            ->join($table, $table . '.id', '=', 'product.' . $table . '_id')
            ->distinct()
            ->get();

        $date = date('Y-m-d');
        // recuperation de 5 gooddeal où le produit rattaché a les meilleures notes
        /*$carousel =  DB::table('gooddeal')
            ->select(DB::raw('gooddeal.name as name, gooddeal.id as id, gooddeal.'.$carnelStartDate.' as startDate,
              gooddeal.'.$carnelEndDate.' as endDate, product.image as image, gooddeal.'.$carnelProductId.' as productid'))
            ->join('product', 'gooddeal.' .$productid.'', '=', 'product.id')
            ->where('gooddeal.'.$enddate,'>=', $date)
            ->orderBy('product.grade', 'desc')
            ->limit(5)
            ->get();
            //dd($date);
*/
        $carousel  = DB::connection()->select(DB::raw(
            'SELECT gooddeal.id as id, product.image as image, gooddeal.name as name, gooddeal.'.$carnelStartDate.' as startDate,
              gooddeal.'.$carnelEndDate.' as endDate,gooddeal.'.$carnelProductId.' as productid
            FROM gooddeal
                JOIN product on(gooddeal.'.$carnelProductId.' = product.id)
                JOIN shop on(product.shop_id = shop.id)
             WHERE state = ' . $status . '
             AND DATE(gooddeal.'.$carnelEndDate.') >= DATE(NOW())
             ORDER BY product.grade desc
             LIMIT 5'
        ));
        //dd($carousel);

        // prepare columns options for datatable
        $aColumnsOptions = [
            [
                'title' => 'Id',
                'name' => 'id',
                'type' => 'text',
                'searchable' => false,
                'visible' => false
            ],
            [
                'title' => 'Image',
                'name' => 'image',
                'type' => 'hidden',
                'searchable' => false,
                'visible' => false

            ],
            [
                'title' => 'Nom',
                'name' => 'name',
                'type' => 'text',
                'searchable' => true,
                'visible' => false
            ],
            [
                'title' => 'Description',
                'name' => 'description',
                'type' => 'text',
                'searchable' => false,
                'visible' => true
            ],
            [
                'title' => 'StartDate',
                'name' => 'startDate',
                'type' => 'hidden',
                'searchable' => true,
                'visible' => false
            ],
            [
                'title' => 'endDate',
                'name' => 'endDate',
                'type' => 'hidden',
                'searchable' => true,
                'visible' => true
            ],
            [
                'title' => 'Discount',
                'name' => 'discount',
                'type' => 'int',
                'searchable' => true,
                'visible' => false
            ],
            [
                'title' => 'Price',
                'name' => 'price',
                'type' => 'int',
                'searchable' => true,
                'visible' => false
            ],
            [
                'title' => 'Type',
                'name' => '"' . $Idtype . '"',
                'type' => 'hidden', // voir pour mettre en hidden la colonne
                'searchable' => true,
                'visible' => false
            ],
            [
                'title' => 'Product',
                'name' => '"' . $productid . '"',
                'type' => 'hidden', // voir pour mettre en hidden la colonne
                'searchable' => false,
                'visible' => false
            ],
            [
                'title' => 'Shop',
                'name' => '"' . $shopname . '"',
                'type' => 'hidden', // voir pour mettre en hidden la colonne
                'searchable' => false,
                'visible' => false
            ]
        ];

        return view('goodDeal')
            ->with('Columns', $aColumnsOptions)
            ->with('resultsType', $Types)
            ->with('carousel', $carousel);

    }

    public function detailProduitGoodDeal(Request $request)
    {
        $params = $request;

        $product = Product::Where('id', $params['id'])->firstOrFail();
        $reviews = review::where('product_id', $params['id'])->get();
        //$users = User::where('id', $reviews->user_id)->get();
        $products = Product::where('typeProduct_id', $product->typeProduct_id)->get();

        $product->priceTTC = $params['price'];
        $isGooddeal = true;
        $startDate = $params['startDate'];
        $endDate = $params['endDate'];
        return view('productPage')
            ->with('product', $product)
            ->with('reviews', $reviews)
            ->with('products', $products)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate)
            ->with('isGooddeal', $isGooddeal);

    }


    public function getDetailProduitGoodDeal($id)
    {
        $enddate = "endDate";
        $carnelEndDate = '"' . $enddate . '"';

        $gooddeal  =
            DB::connection()->select(DB::raw(
            'SELECT *
             FROM gooddeal
             WHERE id = '.$id.'
             AND DATE(gooddeal.'.$carnelEndDate.') >= DATE(NOW())'
        ));

        $product = Product::Where('id', $gooddeal[0]->Product_id)->firstOrFail();
        $reviews = review::where('product_id', $gooddeal[0]->Product_id)->get();
        //$users = User::where('id', $reviews->user_id)->get();
        $products = Product::where('typeProduct_id', $product->typeProduct_id)->get();

        $product->priceTTC = $gooddeal[0]->price;
        $isGooddeal = true;
        $startDate = $gooddeal[0]->startDate;
        $endDate = $gooddeal[0]->endDate;
        return view('productPage')
            ->with('product', $product)
            ->with('reviews', $reviews)
            ->with('products', $products)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate)
            ->with('isGooddeal', $isGooddeal);

    }

}
