<?php

namespace App\Http\Controllers;

use App\TownHall;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class townHallController extends Controller
{
    /**
     * get home view
     */
    public function index()
    {
        $output = $this->getInfoShops();
        return view('AccueilMairie', $output);
    }

    /**
     * get information of shop sorted by state
     */
    public function getInfoShops()
    {
        $townhall = TownHall::where('id', 1)->get();
    
        $allShops = DB::connection()->select(DB::raw(
            'SELECT shop.id as id, shop.name as name, entity.address as addresse, shop.image as image, shop.state as state 
             FROM shop JOIN entity on(shop.entity_id = entity.id)'
        ));

       $columns = $allShops;
       //dd($columns);
        
        $validShops = Shop::select('*')
        ->where('townhall_id', $townhall[0]->id)
        ->where('state', 'Validate')
        ->get();

        $outstandingShops = Shop::select('*')
        ->where('townhall_id', $townhall[0]->id)
        ->where('state', 'Waiting')
        ->get();

        $unSetShops = Shop::select('*')
        ->where('townhall_id', $townhall[0]->id)
        ->where('state', 'Disabled')
        ->get();

        $output = [
            "columns" => $columns,
            "townhall" => $townhall,
            "validate" => $validShops,
            "outstanding" => $outstandingShops,
            "unset" =>  $unSetShops
        ];

        return $output;
    }



}
