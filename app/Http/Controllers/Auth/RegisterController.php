<?php

namespace App\Http\Controllers\Auth;

use App\Entity;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:entity'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Entity
     */
    protected function create(array $data)
    {


        $entity = Entity::Create([
            'city' => 'wait',
            'zipCode' => 11111 ,
            'address' => 'wait',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'numero' => '0000000000',
        ]);


        User::Create([
            'lastname' => $data['name'],
            'firstname' => 'Do' ,
            'rightReview' => True ,
            'entity_id' => $entity->id ,
        ]);

        return $entity;
    }
}
