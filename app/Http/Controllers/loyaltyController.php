<?php

namespace App\Http\Controllers;

use App\Fidelity;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\DB;

class LoyaltyController extends OrderController
{
    protected $state;

    /* construtor to add after cart completed
        public function __construct()
        {
            $this->middleware('auth');
        }
    */

    /**
     * go to loyalty program page
     */

    public function getContent($id, $state = 0)
    {
        return view('loyaltyProgram')->with(
            $this->getLoyaltyProgram($id, $state)
        );
    }

    /**
     * get loyalty program of e-commerce 
     */
    public function getLoyaltyProgram($id, $state = 0)
    {
       
        $loyaltyProgram      = "loyaltyProgram";
        $loyaltyProgram = '"' . $loyaltyProgram . '"';

        //program
        $program = $this->getProgram($id);
        $program_id = $program->program_id;

        //fidelity
        $fidelity = $this->getFidelity($program_id);
        
        // Points
        $targetPoints = $this->getTargetPoints($program_id);
        $countTargets = count($targetPoints) + 1;
        $lastTarget = $this->lastTargetPoint($program_id);

        // confirmation
        $msg = ($state == "ok") ? "Votre adhésion a été enregistrée avec succès ! " : "";

        return [
            'program' => $program,
            'fidelity' => $fidelity,
            'targetPoints' => $targetPoints,
            'countTargets' => $countTargets,
            'lastTarget' => $lastTarget,
            'state' => $state,
            'msg' => $msg
        ];
    }


    /**
     * get lastTargetPoint of the selected loyalty program
     */
    public function lastTargetPoint($id)
    {
        
        $lastTarget = DB::table('pallierProgram')
            ->select(DB::raw('"objectifPoint"'))
            ->where('loyalty_program_id', $id)
            ->orderBy('objectifPoint', 'desc')
            ->first();

        return $lastTarget;

    }

    /**
     * signup current client to fidelity programm
     */
    public function create(Request $request, $shop_id)
    {
        // user_id = Auth::user()->id
        $user_id = 1;
        $fidelity = new Fidelity();
        if ($request->input('signup')) :
            // get program_id
            $programInfos = $this->getLoyaltyProgram($shop_id);
            $program_id = $programInfos['program']->program_id;
            $shop_id = $programInfos['program']->shop_id;

            // create fidelity row
            $fidelity->pointUser = 0;
            $fidelity->user_id = $user_id;
            $fidelity->program_id = $program_id;
            $fidelity->save();

        endif;
        $this->state = "ok";
        return $this->getContent($shop_id, $this->state);
    }

}
