<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDeliveryAddressRequest;
use App\Order;
use Illuminate\Http\Request;

class FormClassicDeliveryController extends Controller
{
    //affiche formulaire d'adresse
    public function index()
    {
        //recupérer l'order d'ou on vient et la renvoyer dans la page
        return view('inscriptionCommerce');
    }

    //store le formulaire de l'addresse
    public function addDeliveryAddress()
    {
        Order::addDeliveryAddress($_POST['user_id'], $_POST['deliveryAddress'], $_POST['city'], $_POST['zipCode'], $_POST['deliveryMode']);
        return redirect(route('paiment'));
        //todo return page de paiement
    }

}
