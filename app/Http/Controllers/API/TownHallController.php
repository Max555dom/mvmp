<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Controllers\townHallController as ControllersTownHallController;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class townHallController extends ControllersTownHallController
{

    /**
     * get listing of shops
     */
    public function getShopsListing($state){
        $output = $this->getInfoShops();
        $status = "'" . $state . "'";
        //$status  = $state ;
     //   dd($status);
        $shops = DB::connection()->select(DB::raw(
            "SELECT shop.id as id, shop.name as name, entity.address as address, shop.image as image, shop.state as state 
             FROM shop JOIN entity on(shop.entity_id = entity.id)
             WHERE state = ".$status
             ));
   
       //dd($columns);
        $return = array('data' => $shops);
        
        return response()->json($return, 200);

    }

}
