<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccueilCommercesController extends Controller
{
    public function index(Request $request)
    {
        //Search data for datatable
        /*
        $sortFieldIndex = $request['order'][0]['column'];
        $sortField      = $request['columns'][$sortFieldIndex]['name'];
        $sortDirection  = $request['order'][0]['dir'];

        $searchField = '';
        if($request['search']['value'] != '') {
            $aSearchParams = array();
            foreach($request['columns'] AS $fieldInfos) {
                if($fieldInfos['searchable'] == "true") {
                    $aSearchParams[] = $fieldInfos['name'].'::text LIKE \'%'.$request['search']['value'].'%\'';
                }
                if(count($aSearchParams) > 0) {
                    $searchField = ' AND ('.implode(' OR ', $aSearchParams).')';
                }
            }
        }
        */
        /*
        $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                              FROM shop
                                                              WHERE 1 = 1' . $searchField));
        $resultsShop  = DB::connection()->select(DB::raw('SELECT *
                                                            FROM shop
                                                            WHERE 1 = 1'.$searchField.'
                                                            AND state = '.$status.'
                                                            ORDER BY '.$sortField.' '.$sortDirection.'
                                                            LIMIT '.$request['length'].'
                                                            OFFSET '.$request['start']));
        */

        $status       = "'Validate'";
        $Idtype      = "typeShop_id";
        $zipCode      = "zipCode";
        $zipCodeFormated = '"' . $zipCode . '"';
        $carnelIdtype = '"'.$Idtype.'"';

        $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                            FROM shop
                                                            WHERE state = '.$status
                                                            ));

        $resultsShop  = DB::connection()->select(DB::raw(
            'SELECT 
                shop.id, 
                shop.image, 
                shop.name, 
                shop.description, 
                shop.state, 
                '.$carnelIdtype.', 
                entity.address, 
                entity.' . $zipCodeFormated . ', 
                entity.city 
            FROM shop 
            JOIN entity on(entity.id = shop.entity_id)
            WHERE state = '.$status
            ));
        
        $dataShop  = array();
        foreach($resultsShop as $oneShop) {
            $temp = (array)$oneShop;
            array_push($dataShop, array_values($temp));
        }
            
        $return = array('draw'            => $request['draw'],
                        'recordsTotal'    => $resultsTotal[0]->total,
                        'recordsFiltered' => $resultsTotal[0]->total,
                        'data'        => $dataShop,
                        );
        return response()->json($return , 200);
    }

   /* public function filter(Request $request,$idType = -1)
    {
        try {

            if($idType == "all")
            {
                $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                                FROM shop'));
                //Search all Chop on database
                $resultsShop = DB::table('shop')
                                        ->select(DB::raw('*'))
                                        ->where('state', 'Validate')
                                        ->orderBy('id','ASC')
                                        ->get();

            }else{
                $type = "typeShop_id";
                $typeCarnel = '"'.$type.'"';
                $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                                FROM shop
                                                                WHERE '.$typeCarnel.' = '.$idType));
                //Search all Chop on database
                $resultsShop = DB::table('shop')
                                        ->select(DB::raw('*'))
                                        ->where('state', 'Validate')
                                        ->where('typeShop_id', $idType)
                                        ->orderBy('id','ASC')
                                        ->get();
            }

            $dataShop  = array();
            foreach($resultsShop as $oneShop) {
                $temp["id"]          = $oneShop->id;
                $temp["name"]        = $oneShop->name;
                $temp["description"] = $oneShop->description;
                $temp["image"]       = $oneShop->image;
                $temp["type"]        = DB::table('type')
                                                ->select(DB::raw('name'))
                                                ->where('id', $oneShop->typeShop_id)
                                                ->pluck('name');
                array_push($dataShop, $temp);
            }



            $returnedMessage = true;
        } catch (\Throwable $th) {
            $returnedMessage = false;
        }

        $return = array('draw'            => $request['draw'],
                        'recordsTotal'    => $resultsTotal[0]->total,
                        'recordsFiltered' => $resultsTotal[0]->total,
                        'data'            => $dataShop,
                        );
        return response()->json($return, 200);

    }
*/

    public function getProduct(Request $request,$idShop = -1)
    {
        $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                            FROM product
                                                            WHERE shop_id = '.$idShop
                                                            ));


        $price      = "priceTTC";
        $Carnelprice = '"'.$price.'"';
        $type      = "typeProduct_id";
        $Carneltype = '"'.$type.'"';
        $resultsProduct  = DB::connection()->select(DB::raw('SELECT id, image, name,'.$Carnelprice.','.$Carneltype.'
                                                    FROM product
                                                    WHERE shop_id = '.$idShop
                                                    ));
        $dataShop  = array();
        foreach($resultsProduct as $oneShop) {
            $temp = (array)$oneShop;
            array_push($dataShop, array_values($temp));
        }

        $return = array('draw'            => $request['draw'],
                        'recordsTotal'    => $resultsTotal[0]->total,
                        'recordsFiltered' => $resultsTotal[0]->total,
                        'data'        => $dataShop,
                        );
        return response()->json($return , 200);
    }

    public function validateCommerce(Request $request){ # deplacer dans chop
        try {
            DB::table('shop')
              ->where('id', $request->shop_id)
              ->update(['state' => 'Validate']);
        } catch (\Throwable $th) {
            throw new \Exception();
        }

        return response()->json(200);
    }

     public function waiting(Request $request){ # deplacer dans chop
        $shop_id = $request->shop_id;
        $commentaire = $request->commentaire;
        try {
            DB::table('shop')
              ->where('id', $request->shop_id)
              ->update(['state' => 'Waiting','comment' => $request->commentaire]);
        } catch (\Throwable $th) {
            throw new \Exception();
        }

        return response()->json(200);
    }

    public function getStock(Request $request,$idShop = -1)
    {
        $resultsTotal = DB::connection()->select(DB::raw('SELECT COUNT(*) AS total
                                                            FROM product
                                                            WHERE shop_id = '.$idShop
                                                            ));


        $priceTTC     = "priceTTC";
        $CarnelpriceTTC = '"'.$priceTTC.'"';

        $priceHT      = "priceHT";
        $CarnelpriceHT = '"'.$priceHT.'"';



        $type      = "typeProduct_id";
        $Carneltype = '"'.$type.'"';
        $resultsProduct  = DB::connection()->select(DB::raw('SELECT id, name, inventory, '.$CarnelpriceHT.', '.$CarnelpriceTTC.', reference, '.$Carneltype.'
                                                    FROM product
                                                    WHERE shop_id = '.$idShop
                                                    ));
        $dataShop  = array();
        foreach($resultsProduct as $oneShop) {
            $temp = (array)$oneShop;
            array_push($dataShop, array_values($temp));
        }
        $return = array('draw'            => $request['draw'],
                        'recordsTotal'    => $resultsTotal[0]->total,
                        'recordsFiltered' => $resultsTotal[0]->total,
                        'data'        => $dataShop,
                        );
        return response()->json($return , 200);
    }

}
