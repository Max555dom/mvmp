<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProduitsCommerceController extends Controller
{
    public function index(Request $request)
    {
        // parameters used into the sql queries
        $status       = "'Validate'";
        $Idtype      = "typeProduct_id";
        $price = "priceTTC";
        $carnelIdtype = '"' . $Idtype . '"';
        $carnelPrice = '"' . $price . '"';

        $resultsTotal = DB::connection()->select(DB::raw(
            'SELECT COUNT(*) AS total FROM product JOIN shop on(product.shop_id = shop.id)
             WHERE shop.state = ' . $status
        ));

        $resultsProduct  = DB::connection()->select(DB::raw(
            'SELECT product.id as id, product.image as image, product.name as name, shop.name as subname, product.description as description, shop.state as state, ' . $carnelIdtype . ',  ' . $carnelPrice . ', shop_id
             FROM product JOIN shop on(product.shop_id = shop.id)
             WHERE state = ' . $status . '
             ORDER BY shop_id,  '.$carnelPrice.' '
        ));

        $dataProduct  = array();
        foreach ($resultsProduct as $oneProduct) {
            $temp = (array)$oneProduct;
            array_push($dataProduct, array_values($temp));
        }

        $return = array(
            'draw'            => $request['draw'],
            'recordsTotal'    => $resultsTotal[0]->total,
            'recordsFiltered' => $resultsTotal[0]->total,
            'data'        => $dataProduct,
        );
        return response()->json($return, 200);
    }



    
}
