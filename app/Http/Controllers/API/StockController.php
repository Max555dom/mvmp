<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    
        
    //for render the image on live 
    /*
    public function image(Request $request)
    {
      try {
        $name         = $_FILES['file']['name'];
        $file_Content = $_FILES['file']['tmp_name'];//tmp_name is the path server of file
        move_uploaded_file($file_Content, "../storage/app/public/ImageRender/" . $name); // 
      } catch (\Throwable $th) {
        throw new \Exception();    
      }

      return response()->json(200);
   
    }
    */

    public function add(Request $request)
    {
      try {
          //On récupére les données de notre formulaire 
          $date = date('Y/m/d h:i:s', time());
          $id_shop = $request['idShop'];
          $product_name = $request['name'];
          $type = $request['type'];
          $ref = $request['ref'];
          $commentaire = $request['commentaire'];
          $ht = $request['ht'];
          $ttc = $request['ttc'];
          $brand = $request['brand'];
          $inventory = $request['inventory'];

          //On recherche si le type de produit renseigner existe en base 
          $idType = DB::table('typeProduct')
              ->select(DB::raw('id'))
              ->where('name', $type)
              ->get(); 
          
          //Sinon on le créée
          if(!isset($idType[0])){ 
            DB::table('typeProduct')->insert([
              'name'           => $type,
              'created_at'        => date($date),
              'updated_at'       => date($date),
            ]);
            $idType = DB::table('typeProduct')
              ->select(DB::raw('id'))
              ->where('name', $type)
              ->where('created_at', date($date))
              ->where('updated_at', date($date))
              ->get();
          }
          // on créée notre produit avec nos information
          DB::table('product')->insert([
            'name' => $product_name,
            'description' => $request['commentaire'],
            'grade' => '0',
            'priceTTC' => $request['ttc'],
            'priceHT' => $request['ht'],
          
            'brand' => $request['brand'],
            'inventory' => $request['inventory'],
            'typeProduct_id' => $idType[0]->id,
            'shop_id' => $id_shop,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);
        // on recherche l'id du produit pour la construction de la référence et du nom de son image 
        $id_product = DB::table('product')
              ->select(DB::raw('id'))
              ->where('name', $product_name)
              ->where('shop_id',$id_shop)
              ->where('created_at', date($date))
              ->where('updated_at', date($date))
              ->pluck('id')[0]; 
        
        //Script d'enrigtrement de l'image
        $countfiles         = count($_FILES['files']['name']);//Count total files
        $tabDocumentVersion = array();
        // Loop all files 
        for($index = 0; $index < $countfiles; $index++){
          $file_Content = $_FILES['files']['tmp_name'][$index];//tmp_name is the path server of file
          move_uploaded_file($file_Content, "../storage/app/public/ProductsImages/Product_" .$id_shop."_".$id_product.".png" ); 
        }
        
        //On renseigne l'image et la référence sur le produit 
        DB::table('product') 
        ->where('id',$id_product)
        ->where('shop_id',$id_shop)
        ->update(['reference' => $ref,
                  'image' => "Product_" .$id_shop."_".$id_product.".png"
          ]);

      } catch (\Throwable $th) {
        throw new \Exception();  
      }

      return response()->json(200);
    }

    public function update(Request $request)
    {
      try {
        $inventory = $request['inventory'];
        $id_product = $request['id_product'];
  
        //On actualise le stock du produit 
        DB::table('product') 
        ->where('id',$id_product)
        ->update(['inventory' => $inventory
          ]);
       
        
      } catch (\Throwable $th) {
        throw new \Exception(); 
      }

      return response()->json(200);
    }




    public function addProgramme(Request $request){
      try {
        $name_programme = $request['name_programme'];
        $description = $request['description'];
        $idShop = $request['idShop'];
        $pointsByCommande = $request['pointsByCommande'];
        $minPriceByCommande = $request['minPriceByCommande'];
        
        //Script d'enrigtrement de l'image
        $countfiles         = count($_FILES['files']['name']);//Count total files
        // Loop all files 
        for($index = 0; $index < $countfiles; $index++){
          $file_Content = $_FILES['files']['tmp_name'][$index];//tmp_name is the path server of file
          move_uploaded_file($file_Content, "../storage/app/public/ImagesShopProgram/Programme_" .$idShop.".png" ); 
        }

        $date = date('Y/m/d h:i:s', time());
        DB::table('loyaltyProgram')->insert([
            'name'           => $name_programme,
            'description'    => $description,
            'image'          => "Programme_" .$idShop.".png" ,
            'minPriceByCommande' => $minPriceByCommande,
            'pointCommande'           => $pointsByCommande,
            'shop_id'    => $idShop,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    
      } catch (\Throwable $th) {
          throw new \Exception(); 
      }
     
      return response()->json(200);
    }
  


    public function addPallier(Request $request){
      try {
        ///On récupére les données de notre formulaire 
        $date = date('Y/m/d h:i:s', time());
        $name_pallier = $request['name_pallier'];
        $objectif = $request['objectif'];
        $bonus = $request['bonus'];
        $commentaire = $request['commentaire'];
        $idProgramme = $request['idProgramme'];
        
        // on créée notre pallier avec nos information
        
        DB::table('pallierProgram')->insert([
              'name'               => $name_pallier,
              'description'        => $commentaire,
              'objectifPoint'      => $objectif,
              'bonus'              =>  $bonus,
              'loyalty_program_id'     => $idProgramme,
              'created_at'        => date($date),
              'updated_at'       => date($date),
          ]);
      } catch (\Throwable $th) {
        throw new \Exception(); 
      }
      return response()->json(200);
    }


    public function editProgramme(Request $request){
      try {
        $name_programme = $request['name_pallier_edit'];
        $description = $request['description_edit'];
        $idProgramme = $request['idProgramme'];

        //On actualise les champs du programme
        DB::table('loyaltyProgram') 
        ->where('id',$idProgramme)
        ->update(['name'        => $name_programme,
                  'description' => $description
          ]);
      } catch (\Throwable $th) {
        throw new \Exception(); 
      }
      return response()->json(200);
    }

    public function deleteProgramme(Request $request){
      try {
        $idProgramme = $request['idProgramme'];
        $date = $request['date'];

        $datedebut = strtotime($date);
        $datefin = $datedebut + 86400 * 60;
        //$datefin = date('Y-m-d H:i:s', $datefin);

        $datenow = date('Y/m/d h:i:s', time());
        $datenow = strtotime($datenow);

        if($datedebut <= $datenow && $datenow >= $datefin){
        
          DB::table('pallierProgram') 
          ->where('loyalty_program_id','=',$idProgramme)
          ->delete();
          
          DB::table('loyaltyProgram') 
          ->where('id','=',$idProgramme)
          ->delete();
          
        }else{
          throw new \Exception(); 
        }
        
        
      } catch (\Throwable $th) {
        throw new \Exception(); 
      }
      return response()->json(200);
    }


    public function deletePallier(Request $request){
      try {
        $idPallier = $request['idPallier'];
        DB::table('pallierProgram') 
          ->where('id','=',$idPallier)
          ->delete();
      } catch (\Throwable $th) {
        throw new \Exception(); 
      }
      return response()->json(200);
    }

    
    
}
