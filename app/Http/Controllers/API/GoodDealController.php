<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GoodDealController extends Controller
{
    public function index(Request $request)
    {
        // parameters used into the sql queries
        $productid = "Product_id";
        $startdate = "startDate";
        $enddate = "endDate";
        $Idtype = "typeProduct_id";
        $typeproduct ="typeProduct";
        $carnelProductId = '"' . $productid . '"';
        $carnelStartDate = '"' . $startdate . '"';
        $carnelEndDate = '"' . $enddate . '"';
        $carnelType = '"' . $Idtype . '"';
        $carnelTypeProduct = '"' . $typeproduct . '"';
        $status       = "'Validate'";

        $resultsTotal = DB::connection()->select(DB::raw(
            'SELECT COUNT (*) AS total
            FROM gooddeal
                INNER JOIN product on gooddeal.'.$carnelProductId.' = product.id
                INNER JOIN shop on product.shop_id = shop.id
            WHERE shop.state = '.$status
        ));

        $resultsGoodDeal  = DB::connection()->select(DB::raw(
            'SELECT gooddeal.id as id, gooddeal.image as image, gooddeal.name as name, gooddeal.description as description, gooddeal.'.$carnelStartDate.' as startDate,
              gooddeal.'.$carnelEndDate.' as endDate, gooddeal.discount as discount, gooddeal.price as price,  product.'.$carnelType.' as type, gooddeal.'.$carnelProductId.', shop.name as shopName
             FROM gooddeal JOIN product on(gooddeal.'.$carnelProductId.' = product.id)
                JOIN shop on(product.shop_id = shop.id)
                JOIN '.$carnelTypeProduct.' on(product.'.$carnelType.' = '.$carnelTypeProduct.'.id)
             WHERE state = ' . $status . '
             AND DATE(gooddeal.'.$carnelEndDate.') >= DATE(NOW())
             ORDER BY gooddeal.price '
        ));

        $dataGoodDeal  = array();
        foreach ($resultsGoodDeal as $oneGoodDeal) {
            $temp = (array)$oneGoodDeal;
            array_push($dataGoodDeal, array_values($temp));
        }


        $return = array(
            'draw'            => $request['draw'],
            'recordsTotal'    => $resultsTotal[0]->total,
            'recordsFiltered' => $resultsTotal[0]->total,
            'data'        => $dataGoodDeal,
        );
        return response()->json($return, 200);
    }


}
