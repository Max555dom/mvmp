<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController as ControllersOrderController;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends ControllersOrderController
{
  public function CreateCheckout()
  {
    \Stripe\Stripe::setApiKey('sk_test_51Ihv5aDIqoOKjau7bEjXZ3nHy6OmwAuiMv6dBzKCOZKAlQ10vuS1H03BIzOGCUqtLoUrodwYlbdpGR6lTPrfRbJy00g2KqPGH7'); // Connection au compte MVMP, secréte

    /**
     * 
     * Create a paiment 
     * 
     */
    $payment_intent = \Stripe\PaymentIntent::create([
      'payment_method_types' => ['card'],
      'amount' => 1000,
      'currency' => 'eur',
      'application_fee_amount' => 123,
      'transfer_data' => [
        'destination' => 'acct_1IjnlcC3PfR7BxYJ',
      ],
    ]);


    return view('paiment')
      ->with('payment_intent', $payment_intent);
    /*
                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                      'name' => 'Kavholm rental',
                      'amount' => 1000,
                      'currency' => 'eur',
                      'quantity' => 1,
                    ]],
                    'payment_intent_data' => [
                      'application_fee_amount' => 123,
                      'transfer_data' => [
                        'destination' => 'acct_1IjnlcC3PfR7BxYJ',
                      ],
                    ],
                    'mode' => 'payment',
                    'success_url' => 'http://127.0.0.1:8000/paiment', // url of success of the paiment
                    'cancel_url' => 'http://127.0.0.1:8000/', // url if the paiment is cancel ( view panier)
                  ]);
                  */
    /*
                *
                * Created an account link
                * la réponse contient une clé url , redirection de l'utilisateur vers l'url
                *
                 $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1Ijme4IpJswWHGyM',
                    'refresh_url' => 'https://example.com/reauth', // rediriger vers l'authentification en cas de refresh
                    'return_url' => 'https://example.com/return', // rediriger vers l'accueil de mvmp  apres la redirection de l'utilisateur vers la clé url retournée
                    'type' => 'account_onboarding',
                  ]);
                  
                  $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1Ijmi4AANTn7XCpp',
                    'refresh_url' => 'https://example.com/reauth',
                    'return_url' => 'https://example.com/return',
                    'type' => 'account_onboarding',
                  ]);

                  $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1IjmkvFrpDmvwf1U',
                    'refresh_url' => 'https://example.com/reauth',
                    'return_url' => 'https://example.com/return',
                    'type' => 'account_onboarding',
                  ]);

                    */



    /*
                *   compte = compte stripe
                *   Création du compte connectés au compte principal de MVMP
                *
                $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce1@gmail.com',
                    'type' => 'standard',
                  ]);

                 $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce2@gmail.com',
                    'type' => 'standard',
                  ]);

                 $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce3@gmail.com',
                    'type' => 'standard',
                  ]);
                */
  }

  public function Secret()
  {

    \Stripe\Stripe::setApiKey('sk_test_51Ihv5aDIqoOKjau7bEjXZ3nHy6OmwAuiMv6dBzKCOZKAlQ10vuS1H03BIzOGCUqtLoUrodwYlbdpGR6lTPrfRbJy00g2KqPGH7'); // Connection au compte MVMP, secréte

    $payment_intent = \Stripe\PaymentIntent::create([
      'payment_method_types' => ['card'],
      'amount' => 1000,
      'currency' => 'eur',
      'application_fee_amount' => 123,
      'transfer_data' => [
        'destination' => 'acct_1IjnlcC3PfR7BxYJ',
      ],
    ]);




    return view('secret')
      ->with('payment_intent', $payment_intent);
  }

  public function validateCommande(Request $request)
  {
    try {
      $id_connected_user = 1;
      $tabId = unserialize(rawurldecode($request['tabId']));
      $orders = unserialize(rawurldecode($request['orders']));
      $i = 0;
      foreach ($tabId  as $onecommande) { //Update the orders of the connected customers 
        DB::table('order')
          ->where('order.user_id', '=', $id_connected_user)
          ->where('order.id', '=', $onecommande->id)
          ->update([
            'state' => "PaidByUser",
            'priceTTC' =>  $orders[$i]['order_price'],
            'priceHTC' =>  $orders[$i]['order_priceHT']
          ]);
        // update of userPoint
        $this->calcUserPointAndUpdate($onecommande->id);
        $i++;
      }
    } catch (\Throwable $th) {
      throw new \Exception();
    }

    return response()->json(200);
  }

  /**
   * calculation of user point and update
   */
  public function calcUserPointAndUpdate($id)
  {

    // 0 : get order with all infos updated
    $obj = $this->getOrder($id);
    // 1 : check if the user has joined the loyalty program with shop id
    $program_id = $this->getProgram($obj->shop_id)->program_id;
    $minPriceByCommande = $this->getProgram($program_id)->minPriceByCommande;
    $fidelity = $this->getFidelity($program_id);
    
    // if yes
    if (count($fidelity) > 0) :
     
      // 2 : get pointUser, total amount of the order and minimum price per commande of target Point
      $pointUser =  $fidelity[0]->pointUser;
      $orderPrice = $obj->priceTTC;
      // 3 : if total amount of the order > = minPricebyCommande
      // if yes
      if ($orderPrice >= $minPriceByCommande) :  
        $pointPerOrder = $this->getProgram($program_id)->pointCommande;
        // 4 : adding minPricebyCommande value to userPoint 
        $pointUser +=  $pointPerOrder;
        $this->updateUserPoint($fidelity[0]->id, $pointUser);
        // if not  
      endif;
    // do nothing
    else :
      return 0;
    endif;
  }

  /**
   * update user points
   */
  public function updateUserPoint($id, $value)
  {
    try {
      DB::table('fidelity')
        ->where('fidelity.id', '=', $id)
        ->update(['pointUser' => $value]);
    } catch (\Throwable $th) {
      throw new \Exception();
    }
  }

    /**
     * get order from id
     */
    public function getOrder($id)
    {
        $order = DB::table('order')
            ->select(DB::raw('*'))
            ->where('id', $id)
            ->first();
            
        return $order;
    }

}
