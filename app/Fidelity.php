<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fidelity extends Model
{
    protected $table='fidelity';
    protected $fillable =['PointUser','created_at','updated_at','user_id','program_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

}
