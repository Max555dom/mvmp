<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table='shop';
    protected $fillable =['id','name','siret','siren','logo','image','click&collect','deliveryPoint','deliveryHome','state','FBlink','Instalink','YTlink','grade','colorPage','typoPage','description','intro','hours','comment','typeShop_id','entity_id'];
    public $timestamps = true;



}
