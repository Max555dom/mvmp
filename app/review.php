<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class review extends Model
{
    protected $table = "review";

    protected $fillable = ['id','user_id', 'shop_id', 'product_id','title',  'description', 'grade','answer'];





    public function product()
    {
        return $this->hasMany('App\Product', 'id', 'product_id');
    }
}
