<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Attachment extends Authenticatable
{
    protected $table='attachment';
    protected $fillable =['id','doc','shop_id'];
    public $timestamps = true;
}
