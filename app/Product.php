<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    protected $table = "product";

    protected $fillable = ['id','name',  'description', 'grade','prixTTC', 'prixHT', 'TVA','brande','inventory', 'SellInventory', 'typeProduct_id', 'image'];


    public function typeProduct(): HasOne
    {
        return $this->hasOne('App\typeProduct', 'id', 'typeProduct_id');
    }


}
