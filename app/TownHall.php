<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TownHall extends Model
{
    protected $table='townHall';
    protected $fillable =['id','name','image','entity_id'];
}
