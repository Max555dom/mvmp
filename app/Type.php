<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Type extends Authenticatable
{
    protected $table='type';
    protected $fillable =['id','name'];
    public $timestamps = true;
}
