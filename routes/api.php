<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Display commerces and commerce products
Route::get('/commerces', 'API\AccueilCommercesController@index');
Route::get('/commerces/{idType}', 'API\AccueilCommercesController@filter'); //inactif
Route::get('/commerce/{idShop}', 'API\AccueilCommercesController@getProduct');

Route::post('/validateCommande', 'API\OrderController@validateCommande');

//GoodDeal
Route::get('/listeBonPlan', 'API\GoodDealController@index');

//Commerce validation
Route::post('/waiting', 'API\AccueilCommercesController@waiting');
Route::post('/validate', 'API\AccueilCommercesController@validateCommerce');

//Paiment
Route::post('/validateCommande', 'API\OrderController@validateCommande');

//Render the image on live on create artcicle page
//Route::post('/image-upload', 'API\StockController@image');


//Comerce stock
Route::get('/commerce/stock/{idShop}', 'API\AccueilCommercesController@getStock');
Route::post('/add', 'API\StockController@add');
Route::post('/update', 'API\StockController@update');

//Commerce fidelity
//Programme
Route::post('/add/programme', 'API\StockController@addProgramme');
Route::post('/edit/programme', 'API\StockController@editProgramme');
Route::post('/delete/programme', 'API\StockController@deleteProgramme');

//Palliers
Route::post('/add/programme/pallier', 'API\StockController@addPallier');
Route::post('/delete/programme/pallier', 'API\StockController@deletePallier');



//For stipe purchase
//Route::post('/create-checkout-session', 'API\OrderController@CreateCheckout');
//Route::get('/secret', 'API\OrderController@Secret');

Route::get('/produits', 'API\ProduitsCommerceController@index');
Route::get('/produits/{idType}', 'API\ProduitsCommerceController@filter');

// Mairie
Route::get('/townHall/shops/{state}', 'API\townHallController@getShopsListing')->name('shopListing');

// report
Route::post('/report/shop/{id}', 'API\reportController@createReport')->name('createReport');
