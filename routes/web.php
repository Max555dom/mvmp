<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/commerces', 'AccueilCommercesController@index')->name('accueil-commerces');
Route::get('/commerce/{id_shop}', 'AccueilCommercesController@getShop');

//listing commande
Route::get('/order/{id}', 'OrderController@index')->name('orderList');
Route::post('/order','OrderController@storeOrder')->name('storeOrder');
Route::get('/order','OrderController@DeliveryMode')->name('storeDeliveryMode');

//Product
Route::get('/product','ProductController@index')->name('listProduct');
Route::get('/product/{id}', 'ProductController@details')->name('detailProduct');

//Products
Route::get('/products','ProductController@listProductsShop')->name('listProductsShop');

//Review
Route::get('/review','ReviewController@index')->name('listReview');
Route::get('/review/{id}','ReviewController@detail')->name('detailReview');
Route::post('/review', 'ReviewController@store')->name('storeReview');

//Shopping Basket
Route::resource('panier', 'CartController')->only(['index', 'store', 'update', 'destroy']);

//Formulaire de livraison
Route::get('/formClassicDelivery', 'FormClassicDeliveryController@index')->name('formClassicDelivery');
Route::post('/formClassicDelivery', 'FormClassicDeliveryController@addDeliveryAddress')->name('addDeliveryAddress');

Route::get('/paiment', 'OrderController@getPaiment')->name('paiment');

Route::get('/commerceValidation/{id_shop}', 'AccueilCommercesController@commerceValidation')->name('commerceValidation');

//Inscription Commerce
Route::get('/inscriptionCommerce', 'InscriptionCommerceController@index')->name('inscriptionCommerce');
Route::post('/inscriptionCommerceOK', 'InscriptionCommerceController@inscriptionCommerce')->name('inscriptionCommerceForm');
Route::get('/inscriptionCommerce/{id}', 'InscriptionCommerceController@waitingValue')->name('waitingValue');

//Profil Commerce
Route::get('/profilCommerce/{id}', 'ProfilCommerceController@index')->name('profilCommerce');
Route::post('/profilCommerceOk', 'ProfilCommerceController@updateProfilCommerce')->name('updateProfilCommerceForm');

//Route Arthur

Route::get('/download/{file}', 'AccueilCommercesController@getDownload');
Route::get('/commerceStock/{id_shop}', 'AccueilCommercesController@commerceGetStock');

Route::get('/commerceStock/create/{id_shop}', 'AccueilCommercesController@commerceStockCreate');
Route::get('/commerceStock/update/{id_product}', 'AccueilCommercesController@commerceStockUpdate');

Route::get('/programeFidelity/{id_product}', 'AccueilCommercesController@programeFidelity');


//Bon Plan
Route::get('/listeBonPlan', 'GoodDealController@index')->name('goodDeal');
Route::post('/product', 'GoodDealController@detailProduitGoodDeal')->name('detailProduitGoodDeal');
Route::get('/productGoodDeal/{id}', 'GoodDealController@getDetailProduitGoodDeal')->name('getDetailProduitGoodDeal');

// google map
Route::get('/map', function () { return view('map'); });
Route::get('/map', 'mapCommercesController@index')->name('mapCommerces');
Route::get('/map/markers', 'mapCommercesController@getDataXml')->name('mapMarkers');

// loyalty
Route::get('/loyalty/{id}', 'loyaltyController@getContent')->name('loyaltyProgram');
Route::post('/loyalty/{id}', 'loyaltyController@create')->name('fidelity.create');

// Mairie
Route::get('/townHall', 'townHallController@index')->name('townHall');
Route::get('/home', 'HomeController@index')->name('home');
