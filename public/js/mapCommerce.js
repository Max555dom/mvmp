let markers = new Array();
let marker;
let theCounter = 0;

// 1. initialisation of the map
function initMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }

}

// 2. geolocation of the map after page loading
function showPosition(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    latlng = new google.maps.LatLng(lat, lng);

    // settings for minimum styles of the map
    var myStyles = [{
        featureType: "poi",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }];
    var myOptions = {
        center: latlng,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        styles: myStyles,
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL
        }
    };

    let map = new google.maps.Map(document.getElementById('map'), myOptions);
    let geocoder = new google.maps.Geocoder();

    // Change this depending on the name of your PHP or XML file
    downloadUrl('http://localhost:8000/map/markers', function (data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName('marker');
        Array.prototype.forEach.call(markers, function (markerElem) {
            let id = markerElem.getAttribute('id');
            let name = markerElem.getAttribute('name');
            let address = markerElem.getAttribute('address');
            geocodeAddress(address, name, id, geocoder, map);
        });
    });
}



// 3. geocode the address given and markering it with all infos
function geocodeAddress(address, name, id, geocoder, resultsMap) {
    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            let infoWindow = new google.maps.InfoWindow;
            let marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
            markers.push(marker);
            let infowincontent = document.createElement('div');
            let strong = document.createElement('strong');
            strong.textContent = name;
            infowincontent.appendChild(strong);
            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(document.createElement('br'));

            let text = document.createElement('text');
            let link = document.createElement('a');
            link.setAttribute('href', '/commerce/' + id);
            text.textContent = address;
            link.textContent = id;

            infowincontent.appendChild(text);
            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(link);
            link.innerText = 'Voir le commerce';
            marker.addListener('click', function () {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
            });
        } else {
            initMap();
        }
    });
}

// request the xml data with a callback
function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}

function doNothing() {}


//---------------- Custom functions (getLocation, errors handle) --------------------


// get the address of the clicked button
$(document).on('click', '.shopLoc', function () {
    const addr = $(this).val();
    getLoc(addr);
});

// get the location of the shop
function getLoc(addr) {
    const geocoder = new google.maps.Geocoder();
    let map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(46.232192999999995, 2.209666999999996),
        zoom: 16
    });
    geocoder.geocode({
        address: addr
    }, (results, status) => {
        if (status === "OK") {
            let resultsMap = map;
            resultsMap.setCenter(results[0].geometry.location);
            // Change this depending on the name of your PHP or XML file
            downloadUrl('http://localhost:8000/map/markers', function (data) {
                var xml = data.responseXML;
                var markers = xml.documentElement.getElementsByTagName('marker');
                Array.prototype.forEach.call(markers, function (markerElem) {
                    let id = markerElem.getAttribute('id');
                    let name = markerElem.getAttribute('name');
                    let address = markerElem.getAttribute('address');
                    geocodeAddress(address, name, id, geocoder, map);

                });
            })
        } else {
            map;
        }
    });
};

// display the error of map loading
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred.";
            break;
    }
}
