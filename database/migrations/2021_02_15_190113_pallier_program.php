<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PallierProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallierProgram', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description');
            /*/$table->foreign('idLoyaltyProgram')
                ->references('id')
                ->on('loyaltyProgram')
                ->onDelete('SET NULL');/*/
            $table->integer('objectifPoint');
            $table->string('bonus');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallierProgram');
    }
}
