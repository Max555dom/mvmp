<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
           /*/ $table->foreign('idEntity')
                ->references('id')
                ->on('entity')
                ->onDelete('SET NULL');/*/
            $table->string('lastname');
            $table->string('firstname');
            $table->string('deliveryAddress')->nullable();
            $table->string('bilingAddress')->nullable();
            $table->boolean('rightReview')->true();
            $table->integer('nbReview')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
