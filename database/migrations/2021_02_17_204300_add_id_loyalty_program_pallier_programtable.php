<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdLoyaltyProgramPallierProgramtable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pallierProgram', function (Blueprint $table) {
            $table->unsignedBigInteger('loyalty_program_id');
            $table->foreign('loyalty_program_id')
                ->references('id')
                ->on('loyaltyProgram')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
