<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('state');
            $table->string('deliveryAddress')->nullable();
            $table->string('city')->nullable();
            $table->string('zipCode')->nullable();
            $table->double('priceTTC')->nullable();
            $table->double('priceHTC')->nullable();
            $table->date('deliveryDate')->nullable();
            $table->string('timeSlots')->nullable();
            $table->date('hoursClick&Collect')->nullable();
            $table->date('sendDate')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
