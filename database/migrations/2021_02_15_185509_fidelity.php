<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Fidelity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidelity', function (Blueprint $table) {
            $table->id();
           /*/ $table->foreign('idUser')
                ->references('id')
                ->on('user')
                ->onDelete('SET NULL');
            $table->foreign('idProgram')
                ->references('id')
                ->on('loyaltyProgram')
                ->onDelete('SET NULL');/*/
            $table->integer('pointUser');
            $table->timestamps();
            // lastcheckpoint
            $table->integer('lastPointTarget')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidelity');
    }
}
