<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LoyaltyProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loyaltyProgram', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description');
            $table->string('image');
            $table->integer('minPriceByCommande')->nullable();
            $table->integer('pointCommande')->nullable();

           /*/ $table->foreign('idUser')
                ->references('id')
                ->on('user')
                ->onDelete('SET NULL');
            $table->foreign('idShop')
                ->references('id')
                ->on('shop')
                ->onDelete('SET NULL');/*/

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loyaltyProgram');
    }
}
