<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdShopLoyaltyProgramtable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loyaltyProgram', function (Blueprint $table) {
            $table->unsignedBigInteger('shop_id');
            /*/
             * Modification by Gaia Khouas on 15/05/2021 : 
               $table->unsignedBigInteger('shop_id')->nullable(); 
               $table->foreign('shop_id')
                ->references('id')
                ->on('shop')
                ->onDelete('SET NULL');
            /*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
