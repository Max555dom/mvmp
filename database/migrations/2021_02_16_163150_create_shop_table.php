<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {

            $table->id();
            $table->string('name');
            $table->string('siret');
            $table->string('siren');
            $table->string('logo')->nullable();
            $table->string('image')->nullable();
            $table->boolean('click&collect');
            $table->boolean('deliveryPoint');
            $table->boolean('deliveryHome');
            $table->string('state');
            $table->string('FBlink')->nullable();
            $table->string('Instalink')->nullable();
            $table->string('YTlink')->nullable();
            $table->integer('grade')->nullable();
            $table->string('colorPage')->nullable();
            $table->string('typoPage')->nullable();
            $table->text('description')->nullable();
            $table->string('intro')->nullable();
            $table->string('hours')->nullable();
            $table->text('comment')->nullable();
            $table->string('id_stripe')->nullable();
            $table->timestamps();



        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop');
    }
}
