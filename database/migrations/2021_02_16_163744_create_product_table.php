<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->integer('grade')->nullable();//a verifier demande un tableau de int pour moyenne
            $table->double('priceTTC');
            $table->double('priceHT');
            $table->integer('TVA')->nullable();
            $table->string('image')->nullable();
            $table->string('reference')->unique()->nullable();
            $table->string('brand');
            $table->integer('inventory');
            $table->integer('SellInventory')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
