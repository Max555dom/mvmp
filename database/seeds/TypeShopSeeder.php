<?php

use Illuminate\Database\Seeder;

class TypeShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type')->insert([
            'name'           => 'artisan',
            ]);
        DB::table('type')->insert([
            'name'           => 'boulangerie',
        ]);
        DB::table('type')->insert([
            'name'           => 'boucherie',
        ]);
        DB::table('type')->insert([
            'name'           => 'epicerie',
        ]);
        DB::table('type')->insert([
            'name'           => 'magasin de chaussure',
        ]);
        DB::table('type')->insert([
            'name'           => 'magasin de vetement',
        ]);
        DB::table('type')->insert([
            'name'           => 'magasin d electronique',
        ]);
        DB::table('type')->insert([
            'name'           => 'multi-commerce',
        ]);
        DB::table('type')->insert([
            'name'           => 'papeterie',
        ]);
        DB::table('type')->insert([
            'name'           => 'poissonnerie',
        ]);
        DB::table('type')->insert([
            'name'           => 'tabac',
        ]);
        DB::table('type')->insert([
            'name'           => 'fleuriste',
        ]);

    }
}
