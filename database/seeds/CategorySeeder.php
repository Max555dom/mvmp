<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deliveryMode')->insert([
            'name' => 'Livraison à domicile',
        ]);

        DB::table('deliveryMode')->insert([
            'name' => 'Livraison en point relay',
        ]);

        DB::table('deliveryMode')->insert([
            'name' => 'Click & Collect',
        ]);
    }
}
