<?php

use Illuminate\Database\Seeder;

class TownHallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('townHall')->insert([
            'name' => 'Mairie de Dreux',
            'image' => 'imageMarieDreux.png',
            'entity_id'=>null,
        ]);

    }
}
