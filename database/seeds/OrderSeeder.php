<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('order')->insert([
            'state' => 'en préparation',
            'deliveryAddress'=> '12 rue Haute',
            'city' => 'Velizy',
            'zipCode'=> '78140',
            'priceTTC'=> 8.98,
            'priceHTC' => 7.63,
            'deliveryDate'=>date('2021/03/2 15:15:00', time()),
            'timeSlots'=>null,
            'sendDate'=>null,
            'created_at'=> $date,
            'updated_at'=> $date,
            'shop_id'=>1,
            'user_id'=>1,
            'deliveryMode' => 1,
        ]);

        $date = date('Y/m/d h:i:s', time());
        DB::table('order')->insert([
            'state' => 'en attente de validation',
            'deliveryAddress'=> '20 rue des castor',
            'city' => 'Paris',
            'zipCode'=> '75001',
            'priceTTC'=> 7.75,
            'priceHTC' => 6.68,
            'deliveryDate'=>date('2021/03/2 15:15:00', time()),
            'timeSlots'=>null,
            'sendDate'=>null,
            'created_at'=> $date,
            'updated_at'=> $date,
            'shop_id'=>2,
            'user_id'=>1,
            'deliveryMode' => 2,
        ]);

        $date = date('Y/m/d h:i:s', time());
        DB::table('order')->insert([
            'state' => 'en attende de reception par le client',
            'deliveryAddress'=> '35 avenue des tulipe',
            'city' => 'Nanterre',
            'zipCode'=> '92000',
            'priceTTC'=> 26.74,
            'priceHTC' => 22.82,
            'deliveryDate'=>date('2021/03/2 15:15:00', time()),
            'timeSlots'=>null,
            'sendDate'=>null,
            'created_at'=> $date,
            'updated_at'=> $date,
            'shop_id'=>3,
            'user_id'=>2,
            'deliveryMode' => 3,
        ]);



    }
}
