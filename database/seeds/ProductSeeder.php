<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('product')->insert([
            'name' => 'Yaourt fruit rouge morceaux',
            'description' => 'Les délicieux yaourt bio au lait de vache avec de savoureux morceaux de fruit finement tranché et mixer pour un moment de plaisir intense',
            'grade' => 3,
            'priceTTC' => 2.99,
            'priceHT' => 2.54,
            'TVA' => 15,
            'image'=> 'Product_1_1.png',
            'brand' => 'Paturage',
            'inventory' => 100,
            'SellInventory' => 80,
            'typeProduct_id' => 1,
            'shop_id' => 1,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);


        DB::table('product')->insert([
            'name' => 'Nutella',
            'description' => 'Pâte à tartiner noisette de Ferrero. Elle est composée de sucre, dhuile de palme, de noisettes, de cacao et de lait ',
            'grade' => '0',
            'priceTTC' => 5.99,
            'priceHT' => 5.09,
            'TVA' => 15,
            'image'=> 'Product_1_2.png',
            'brand' => 'Ferrero',
            'inventory' => 80,
            'SellInventory' => 50,
            'typeProduct_id' => 2,
            'shop_id' => 1,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);
        
        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Sac à main - Lancel ',
            'description' => 'Sac à main qui peut se porté à l épaule mais aussi en bandouillère. Sac en cuir de vache made in Italie ',
            'grade' => '0',
            'priceTTC' => 180,
            'priceHT' => 144,
            'TVA' => 20,
            'image'=>  'Product_1_3.png',
            'brand' => 'Lancel',
            'inventory' => 30,
            'SellInventory' => 30,
            'typeProduct_id' => 2,
            'shop_id' => 1,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Baguette aux céréales',
            'description' => 'Les baguettes aux céréales de chez Jacques et Martine. Cuite toute la journée. Céréales et farine bio.',
            'grade' => '0',
            'priceTTC' => 1,
            'priceHT' => 0.95,
            'TVA' => 5,
            'image'=> 'Product_1_4.png',
            'brand' => 'Jacques & Martine',
            'inventory' => 100,
            'SellInventory' => 100,
            'typeProduct_id' => 2,
            'shop_id' => 1,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Truite fumé de norvège',
            'description' => 'Truite fumé de norvège. Pécher par une famille de pecheur au nord de la norvege dans les eaux froides',
            'grade' => '0',
            'priceTTC' => 6.75,
            'priceHT' => 5.73,
            'TVA' => 15,
            'image'=> 'Product_2_5.png',
            'brand' => 'Ovive',
            'inventory' => 80,
            'SellInventory' => 80,
            'typeProduct_id' => 3,
            'shop_id' => 2,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Huile d olive',
            'description' => 'Huile d olive vierge extra doux',
            'grade' => '0',
            'priceTTC' => 6.29,
            'priceHT' => 5.34,
            'TVA' => 15,
            'image'=> 'Product_2_6.png',
            'brand' => 'Puguet',
            'inventory' => 60,
            'SellInventory' => 60,
            'typeProduct_id' => 3,
            'shop_id' => 2,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Papier toilette',
            'description' => 'Papier toilette double épaisseurs',
            'grade' => '0',
            'priceTTC' => 1.99,
            'priceHT' => 1.69,
            'TVA' => 15,
            'image'=> 'Product_2_7.png',
            'brand' => 'Lotus',
            'inventory' => 80,
            'SellInventory' => 80,
            'typeProduct_id' => 3,
            'shop_id' => 2,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Oeuf frais',
            'description' => '6 oeufs fraus moyen bio',
            'grade' => '0',
            'priceTTC' => 2.75,
            'priceHT' => 2.33,
            'TVA' => 15,
            'image'=> 'Product_2_8.png',
            'brand' => 'Label rouge',
            'inventory' => 100,
            'SellInventory' => 80,
            'typeProduct_id' => 4,
            'shop_id' => 2,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Pesto',
            'description' => 'Pesto rustico basilic & courgette 200g',
            'grade' => '0',
            'priceTTC' => 2.99,
            'priceHT' => 2.54,
            'TVA' => 15,
            'image'=> 'Product_3_9.png',
            'brand' => 'Barilla',
            'inventory' => 50,
            'SellInventory' => 50,
            'typeProduct_id' => 5,
            'shop_id' => 3,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);

        DB::table('product')->insert([
            'name' => 'Boucle d oreille anneaux',
            'description' => 'Boucle d oreille anneaux diametre 4 cm en argent fabriquer en france et designer en angletterre',
            'grade' => '0',
            'priceTTC' => 22.99,
            'priceHT' => 19.54,
            'TVA' => 15,
            'image'=> 'Product_3_10.png',
            'brand' => 'Le manège a bijoux ',
            'inventory' => 20,
            'SellInventory' => 20,
            'typeProduct_id' => 5,
            'shop_id' => 3,
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);

        $id = DB::table('product')->max('id');
        DB::table('product')
            ->where('id',$id)
            ->update(['reference' => "REF:".$id]);
    }
}
