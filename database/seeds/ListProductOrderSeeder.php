<?php

use Illuminate\Database\Seeder;

class ListProductOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('list_product_order')->insert([
            'product_id' =>'1',
            'order_id'  => '1',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'2',
            'order_id'  => '1',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'4',
            'order_id'  => '2',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'5',
            'order_id'  => '2',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'4',
            'order_id'  => '3',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'8',
            'order_id'  => '3',
        ]);

        DB::table('list_product_order')->insert([
            'product_id' =>'10',
            'order_id'  => '3',
        ]);

    }
}
