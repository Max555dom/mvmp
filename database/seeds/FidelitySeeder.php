<?php

use Illuminate\Database\Seeder;

class FidelitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('fidelity')->insert([
            'pointUser'           => '20',
            'user_id'    => 1,
            'program_id'          => 1,
            'lastPointTarget'  => '0',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('fidelity')->insert([
            'pointUser'           => '0',
            'user_id'    => 2,
            'program_id'          => 2,
            'lastPointTarget'  => '0',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('fidelity')->insert([
            'pointUser'           => '50',
            'user_id'    => 3,
            'program_id'          => 3,
            'lastPointTarget'  => '50',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    }
}
