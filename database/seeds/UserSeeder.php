<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('users')->insert([
            'lastname' => 'Duval',
            'firstname' =>'Martin',
            'deliveryAddress'           => '1 rue de de belleville',
            'bilingAddress'    => '1 rue de de belleville',
            'rightReview' => true,
            'nbReview'   => 0,
            'remember_token' => Str::random(15),
            'entity_id'     => 1,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('users')->insert([
            'lastname' => 'Blanchard',
            'firstname' =>'Thomas',
            'deliveryAddress'           => '5 rue de de la mairie',
            'bilingAddress'    => '5 rue de de la mairie',
            'rightReview' => true,
            'nbReview'   => 45,
            'remember_token' => Str::random(15),
            'entity_id'     => 2,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('users')->insert([
            'lastname' => 'Rolland',
            'firstname' =>'Garcia',
            'deliveryAddress'           => '17 rue Généralle de Gaulle',
            'bilingAddress'    => '17 rue Généralle de Gaulle',
            'rightReview' => false,
            'nbReview'   => 89,
            'remember_token' => Str::random(15),
            'entity_id'     => 3,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    }
}
