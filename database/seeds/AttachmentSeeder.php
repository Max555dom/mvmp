<?php

use Illuminate\Database\Seeder;

class AttachmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('attachment')->insert([
            'doc' => 'Document_name_Shop_1',
            'shop_id'=> 1,
            'created_at'        => date($date),
            'updated_at'       => date($date)
        ]);

        DB::table('attachment')->insert([
            'doc' => 'Document_name_Shop_2',
            'shop_id'=> 2,
            'created_at'        => date($date),
            'updated_at'       => date($date)
        ]);

        DB::table('attachment')->insert([
            'doc' => 'Document_name_Shop_3',
            'shop_id'=> 3,
            'created_at'        => date($date),
            'updated_at'       => date($date)
        ]);
    }
}
