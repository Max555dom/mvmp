<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());

        DB::table('entity')->insert([ // users
            'city' =>'Dreux',
            'zipCode'  => '28500',
            'address'    => '1 rue de de belleville',
            'email' =>'duval@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.41.36.86",
            'created_at'        => date($date),
            'updated_at'       => date($date),

        ]);

        DB::table('entity')->insert([
            'city'        =>'Dreux',
            'zipCode'           => '28500',
            'address'    => '5 rue de de la mairie',
            'email' =>'blanchard@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.41.36.86",
            'created_at'        => date($date),
            'updated_at'       => date($date),

        ]);

        DB::table('entity')->insert([
            'city'        =>'Dreux',
            'zipCode'           => '28500',
            'address'    => '17 rue Généralle de Gaulle',
            'email' =>'rolland@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.41.36.86",
            'created_at'        => date($date),
            'updated_at'       => date($date),

        ]);
        DB::table('entity')->insert([ // mairie
            'city'        =>'Dreux',
            'zipCode'           => '28500',
            'address'    => '1 rue de la mairie',
            'email' =>'dreux_contact@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.41.36.86",
            'created_at'  => date($date),
            'updated_at'  => date($date),
        ]);

        DB::table('entity')->insert([ // commerces
            'city'        =>'Paris',
            'zipCode'           => '75015 ',
            'address'    => '18 Place du Commerce',
            'email' =>'shop-SushiTeam@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.41.36.86",
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('entity')->insert([
            'city'        =>'Paris',
            'zipCode'           => '75015',
            'address'    => '49 Rue du Commerce',
            'email' =>'shop-pokemon@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.71.36.86",
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('entity')->insert([
            'city'        =>'Paris',
            'zipCode'           => '75008',
            'address'    => '27 Avenue de Friedland',
            'email' =>'shop-WarriorsShop@gmail.com',
            "password" => Hash::make('password'),
            "numero"  => "06.17.56.76",
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    }
}
