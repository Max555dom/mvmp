<?php

use Illuminate\Database\Seeder;

class PallierProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier 1',
            'description'    => '- 20% sur votre prochaine commande',
            'objectifPoint'           => '50',
            'bonus'           => '-20%',
            'loyalty_program_id'     => 1,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier final',
            'description'    => '- 10 euros sur votre prochaine commande',
            'objectifPoint'           => '100',
            'bonus'           => '-5',
            'loyalty_program_id'     => 1,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier argent',
            'description'    => 'Inscription au vente privé',
            'objectifPoint'           => '50',
            'bonus'           => 'vente_priver',
            'loyalty_program_id'     => 2,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier or',
            'description'    => '- 30% sur votre prochaine commande',
            'objectifPoint'           => '100',
            'bonus'           => '-30%',
            'loyalty_program_id'     => 2,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier 1',
            'description'    => 'Livraison gratuite',
            'objectifPoint'           => '5',
            'bonus'           => 'vente_priver',
            'loyalty_program_id'     => 3,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('pallierProgram')->insert([
            'name'           => 'Pallier final',
            'description'    => '- 20 euros sur votre future commande',
            'objectifPoint'           => '100',
            'bonus'           => '-20',
            'loyalty_program_id'     => 3,
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

    }
}
