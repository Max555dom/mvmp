<?php

use Illuminate\Database\Seeder;

class Report extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        
        DB::table('report')->insert([
            'user_id' => '1',
            'shop_id' => '1',
            'message' => 'Je souhaite signaler ce commerce car les yaourts vendus sont périmés.',
            'created_at'=> $date,
            'updated_at'=> $date,

        ]);

        DB::table('report')->insert([
            'user_id' => '1',
            'shop_id' => '2',
            'message' => 'Je souhaite signaler ce commerce car je n\'ai pas reçu tous mes produits.',
            'created_at'=> $date,
            'updated_at'=> $date,
        ]);
    }
}
