<?php

use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('shop')->insert([

            'name' => 'SushiShop',
            'siret' => '34518442800018',
            'siren' => '345184428',
            'logo' => NULL,
            'image' => NULL,
            'click&collect' => true,
            'deliveryPoint' => true,
            'deliveryHome' => true,
            'state' => 'Validate',
            'FBlink' => 'https://www.amazon.de',
            'Instalink' => 'https://www.japscan.se',
            'YTlink' => 'https://www.youtube.com/watch?v=dv13gl0a-FA',
            'grade' => NULL,
            'colorPage' => '#FF0000',
            'typoPage' => NULL,
            'description' => 'Mangasin de vente de sushi communément appellé SushiTeam',
            'intro' => NULL,
            'hours' => '12h - 23h',
            'comment' => 'Trés bon',
            'typeShop_id' => 10,
            'id_stripe' =>'acct_1Ijme4IpJswWHGyM',
            'entity_id' => 5,
            'townhall_id' => 1
        ]);
        DB::table('shop')->insert([

            'name' => 'PokémonShop',
            'siret' => '41863456400011',
            'siren' => '418634564',
            'logo' => 'shopImage.png',
            'image' => 'shopImage2.png',
            'click&collect' => true,
            'deliveryPoint' => true,
            'deliveryHome' => false,
            'state' => 'Validate',
            'FBlink' => 'https://www.amazon.de',
            'Instalink' => 'https://www.japscan.se',
            'YTlink' => 'https://www.youtube.com/watch?v=dv13gl0a-FA',
            'grade' => '5',
            'colorPage' => '#00000',
            'typoPage' => NULL,
            'description' => 'Mangasin de vente de pokemon, Pour tous le attrappé',
            'intro' => NULL,
            'hours' => '9h - 12h 13h - 20h',
            'comment' => 'Bof',
            'typeShop_id' => 1,
            'id_stripe' =>'acct_1Ijmi4AANTn7XCpp',
            'entity_id' => 6,
            'townhall_id' => 1
        ]);
        DB::table('shop')->insert([

            'name' => 'WarriorsShop',
            'siret' => '41863456400011',
            'siren' => '418634564',
            'logo' => 'shopImage2.png',
            'image' => 'shopImage3.png',
            'click&collect' => true,
            'deliveryPoint' => false,
            'deliveryHome' => false,
            'state' => 'Validate',
            'FBlink' => 'https://www.amazon.de',
            'Instalink' => 'https://www.japscan.se',
            'YTlink' => 'https://www.youtube.com/watch?v=dv13gl0a-FA',
            'grade' => '10',
            'colorPage' => '#C0C0C0',
            'typoPage' => NULL,
            'description' => 'Mangasin de vente de goodies pour les Golden Statees Warriors',
            'intro' => 'Hello, voici les warriors dans la place',
            'hours' => '9h - 19h',
            'comment' => 'Trés bon',
            'typeShop_id' => 5,
            'id_stripe' =>'acct_1IjmkvFrpDmvwf1U',
            'entity_id' => 7,
            'townhall_id' => 1
        ]);
    }
}
