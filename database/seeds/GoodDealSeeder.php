<?php

use Illuminate\Database\Seeder;

class GoodDealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('gooddeal')->insert([
            'name'        => "Yaourts",
            'description' => "-10% sur les Yaourts",
            'startDate'   => '2021-05-25',
            'endDate'     => '2021-06-03',
            'quantity'    => 50,
            'discount'    => 10,
            'price'       => 12,
            'image'       => '//placehold.it/200',
            'Product_id'  => 1,
            'created_at'  => date($date),
            'updated_at'  => date($date)
        ]);

        DB::table('gooddeal')->insert([
            'name'        => "Nom promo",
            'description' => "Description Description Description",
            'startDate'   => '2021-05-25',
            'endDate'     => '2021-06-10',
            'quantity'    => 40,
            'discount'    => 50,
            'price'       => 6,
            'image'       => '//placehold.it/200',
            'Product_id'  => 3,
            'created_at'  => date($date),
            'updated_at'  => date($date)
        ]);

        DB::table('gooddeal')->insert([
            'name'        => "Bon plan ",
            'description' => "Description Description Description Description Description Description",
            'startDate'   => '2021-05-28',
            'endDate'     => '2021-08-29',
            'quantity'    => 30,
            'discount'    => 10,
            'price'       => 26,
            'image'       => '//placehold.it/200',
            'Product_id'  => 7,
            'created_at'  => date($date),
            'updated_at'  => date($date)
        ]);

        DB::table('gooddeal')->insert([
            'name'        => "le 3eme offert",
            'description' => "Description Description DescriptionDescription Description DescriptionDescription Description Description",
            'startDate'   => '2021-05-25',
            'endDate'     => '2021-06-29',
            'quantity'    => 50,
            'discount'    => 15,
            'price'       => 8,
            'image'       => '//placehold.it/200',
            'Product_id'  => 4,
            'created_at'  => date($date),
            'updated_at'  => date($date)
        ]);
    }
}
