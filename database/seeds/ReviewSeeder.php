<?php

use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('review')->insert([
            'title'           => 'Avis Produit 1',
            'description'   => 'Alors sans commentaire, la note dira tout',
            'grade' => '1',
            'user_id' => '2',
            'shop_id' => '2',
            'product_id' => '2',
            'answer' => 'Je m en fout ',
        ]);
        DB::table('review')->insert([
            'title'           => 'Avis Produit 2',
            'description'   => 'pas bon, pas beaux',
            'grade' => '3',
            'user_id' => '3',
            'shop_id' => '3',
            'product_id' => '3',
            'answer' => 'Je m en fout pas ',
        ]);
        DB::table('review')->insert([
            'title'           => 'Avis Produit 3',
            'description'   => 'Alors ca va la famille parce que les produit non',
            'grade' => '5',
            'user_id' => '2',
            'shop_id' => '2',
            'product_id' => '1',
            'answer' => 'push push up !!! ',
        ]);
        DB::table('review')->insert([
            'title'           => 'Avis Produit 4',
            'description'   => 'Alors sans commentaire, la note dira tout',
            'grade' => '1',
            'user_id' => '3',
            'shop_id' => '3',
            'product_id' => '2',
            'answer' => 'Je m en fout ',
        ]);
    }
}
