<?php

use Illuminate\Database\Seeder;

class DeliveryCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deliveryMode')->insert([
            'name'           => 'Click&Collect',
        ]);
        DB::table('deliveryMode')->insert([
            'name'           => 'PointDelivery',
        ]);
        DB::table('deliveryMode')->insert([
            'name'           => 'Delivery',
        ]);
    }
}
