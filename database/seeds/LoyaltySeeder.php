<?php

use Illuminate\Database\Seeder;

class LoyaltySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        
        
      
        DB::table('loyaltyProgram')->insert([
            'name'           => 'Le programme city-ready',
            'description'    => 'Le programme city-ready est un programme en 3 pallier principaux de promotion, en adhérant a ce programme vous bénéficierez de nombreuse promotion sur vos achat du quotidient, cumuler sufisant de point pour recevoir directement des bons d achat',
            'image'          => 'programme1.jpg',
            'shop_id'    => 1,
            'minPriceByCommande'           => '0',
            'pointCommande'           => '0',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('loyaltyProgram')->insert([
            'name'           => 'Le programme Your-lux',
            'description'    => "Le programme Your-lux est un programme avec plusieur palliers de promotion, en adhérant a ce programme vous bénéficierez de nombreuse promotion sur vos achat dans notre boutique de plus de 20 euros et tous au long de l'année, cumuler sufisant de et vous serez convier a des ventes privées",
            'image'          => 'programme2.jpg',
            'shop_id'    => 2,
            'minPriceByCommande'           => '0',
            'pointCommande'           => '0',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('loyaltyProgram')->insert([
            'name'           => 'Le programme YourGreen',
            'description'    => "Le programme YourGreen est un programme de fidélité regroupant de nombreuses promotion sur notre catalogue, en adhérant a ce programme vous bénéficierez de la livraison gratuite anssi que des conseille personalisez pour vos plante et espace vert, cumuler sufisant de point pour recevoir directement des bons d'achat",
            'image'          => 'programme2.jpg',
            'shop_id'    => 3,
            'minPriceByCommande'           => '0',
            'pointCommande'           => '0',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    }
}
