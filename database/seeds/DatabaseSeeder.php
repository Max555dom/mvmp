<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([

            EntitySeeder::class,
            UserSeeder::class,
            TypeProductSeeder::class,
            TownHallSeeder::class,
            TypeShopSeeder::class,
            ShopSeeder::class,
            LoyaltySeeder::class,
            FidelitySeeder::class,
            PallierProgramSeeder::class,
            ProductSeeder::class,
            GoodDealSeeder::class,
            DeliveryCategorySeeder::class,
            ReviewSeeder::class,
            OrderSeeder::class,
            ListProductOrderSeeder::class,
            AttachmentSeeder::class,

            ]);
    }
}
