<?php

use Illuminate\Database\Seeder;

class TypeProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y/m/d h:i:s', time());
        DB::table('typeProduct')->insert([
            'name'           => 'patisserie',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'pain',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'viande',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'poisson',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'coquillage',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'soupe',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'produit laitier',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'tubercule',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'boisson',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'alcool',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('typeProduct')->insert([
            'name'           => 'fleur',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'vegetale',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'papier',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'utilitaire',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'tabac',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);

        DB::table('typeProduct')->insert([
            'name'           => 'bijou',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'textile',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'fruit',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'surgelé',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'maroquinerie', // (sac et chaussure)
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
        DB::table('typeProduct')->insert([
            'name'           => 'high-tech',
            'created_at'        => date($date),
            'updated_at'       => date($date),
        ]);
    }
}
