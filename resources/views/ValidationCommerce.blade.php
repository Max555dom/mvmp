@extends('layouts.app')
@section('content')

  <!-- Begin Page Content -->
<div class="container-fluid container-validation">

    <!-- Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center"><?php echo $commerce->shopname;?></h2>
        </div>
        <div class="card-body text-center">

                    <div class="container text-center formValidation">
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label-lg">ADRESSE</label>
                            <div class="col-sm-1">
                                <input id="address" name="address" value="<?php echo $commerce->address;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">CODE POSTAL</label>
                            <div class="col-sm-1">
                                <input id="zipcode" name="zipcode" value="<?php echo $commerce->zipCode;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">TELEPHONE</label>
                            <div class="col-sm-1">
                                <input name="numero" value="<?php echo $commerce->numero;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">TYPE DE COMMERCE</label>
                            <div class="col-sm-1">
                                <input name="name" value="<?php echo $commerce->name;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">SIRET</label>
                            <div class="col-sm-1">
                                <input id='siret' name="siret" value="<?php echo $commerce->siret;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">SIREN</label>
                            <div class="col-sm-1">
                                <input id='siren' name="siren" value="<?php echo $commerce->siren;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">E-MAIL</label>
                            <div class="col-sm-1">
                                <input name="mail" value="<?php echo $commerce->email;?>" readonly/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">DOCUMENT FILE</label>
                            <div class="col-sm-1">
                                <?php
                                    if($commerce->doc != null or $commerce->doc != "")
                                    {
                                ?>
                                        <a href="/download/<?php echo $commerce->doc;?>" class="btn btn-success"><i class="fa fa-download"></i>Download</a>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">IMAGE</label>
                            <div class="col-sm-3">
                                <?php
                                    //echo $commerce->image;
                                    if($commerce->image != null)
                                    {
                                ?>
                                        <img src="{{ asset('storage/ImagesShop/'.$commerce->image) }}" class='img-fluid' alt=''>
                                <?php
                                    }else{

                                ?>
                                        <img src="//placehold.it/200" class='img-fluid' alt=''>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg"> CLICK & COLLECT</label>
                            <div class="col-sm-1">
                                <?php
                                    if($commerce->clickcollect == True)
                                    {
                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" onclick="return false;" checked>
                                <?php
                                    }else{

                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" onclick="return false;">
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">LIVRAISON POINT RELAIS</label>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" checked>
                                <?php
                                    if($commerce->deliveryPoint == True)
                                    {
                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" onclick="return false;" checked>
                                <?php
                                    }else{
                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" onclick="return false;">
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label  class="col-sm-2 col-form-label-lg">LIVRAISON A DOMICILE</label>
                            <div class="col-sm-1">
                                <?php
                                    if($commerce->deliveryHome == True)
                                    {
                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" onclick="return false;" checked>
                                <?php
                                    }else{
                                ?>
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" onclick="return false;">
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>

                    <button type="submit" data-toggle="modal" data-target="#modal-waiting" class="btn btn-lg btn-warning btn-wait">Mettre en attente</button>

                    <button type="submit" class="btn btn-lg btn-primary btn-api">Vérification siren & siret</button>

                    <button type="submit"  data-toggle="modal" data-target="#modal-success" class="btn btn-lg btn-success btn-validate">Valider</button>


        </div>
    </div>
</div>
<div class="modal" id='modal-waiting' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mise en attente du commerce</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Veuillez indiquer au commerçant les raisons de la mise en attente de son commerce</p>
        <textarea id="commentaire" name="commentaire" rows="5" cols="60" maxlength="255" required></textarea>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="shop_id" value="<?php echo $commerce->id;?>" />
        <button type="submit" data-dismiss="modal" class="btn btn-success btn-waiting-shop" >Valider</button>

      </div>
    </div>
  </div>
</div>
<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Validation du commercant</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Etes vous sure de vouloir valider ce commercant ?</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="shop_id" value="<?php echo $commerce->id;?>" />
        <button type="submit" data-dismiss="modal" class="btn btn-success btn-validate-shop" >Valider</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css">


.formValidation{
    width: 100%;
    margin-left: 35%;
}
.btn-wait{
    margin-right: 3%;
}
.btn-api{
    margin-left: 5%;
}
.btn-validate{
    margin-left: 7%;
}

.container-validation{
    width: 70%;

}
</style>
<script>

// Call the dataTables jQuery plugin
$(document).ready(function() {
//Display the menu on side-bar
$('#collapseNavigation').collapse();

//bouton de validation du shop
$(".btn-validate-shop").click(function(e){
    var shop_id = document.getElementById('shop_id').value;
    $.ajax({
        url: '/api/validate',
        type: 'POST',
        data: {"shop_id":shop_id},
        success: function(data) {
          alert("Le commerce à était validée.");
          return true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Erreur dans la validation du commerce.');
        }
    });
  });

//bouton de mise en attente du shop
  $(".btn-waiting-shop").click(function(e){
    var shop_id = document.getElementById('shop_id').value;
    var commentaire = document.getElementById('commentaire').value;
    $.ajax({
        url: '/api/waiting',
        type: 'POST',
        data: {"shop_id":shop_id,"commentaire":commentaire},
        success: function(data) {
          alert("Le commerce à bien était mit en attente.");
          return true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Echec de la mise a jour du statut du commerce, veuillez renseigner les raisons de cette mise en attente.');
        }
    });
  });

//bouton de vérification du code postal est du siren et du siret.
  $(".btn-api").click(function(e){
    var zipcode = document.getElementById('zipcode').value;
    var siren = document.getElementById('siren').value;
    var siret = document.getElementById('siret').value;
    $.ajax({
        url: 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/?code_postal=' + zipcode + '&siren=' + siren + '&siret=' + siret ,
        type: 'GET',
        success: function(data) {
          alert("Le commerce à bien était vérifier.");
          return true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Le couple d'information 'code postal, siret, siren' est inconnu.");
            return false;
        }
    });
  });


});
</script>
@endsection
