@extends('layouts.app')
@section('content')
<style>
.point{
    position:absolute; 
    margin-top:-10px; 
    z-index:1; 
    height:40px; 
    width:40px; 
    border-radius:25px; 
}

.primary-color{
	background-color:#4989bd;
}

.no-color{
	background-color:inherit;
}

.progress{
    background-color:rgb(180, 180, 180);
    width:100%;
  
 
}

.title{
    top:60px;
    position: relative;
    font-size:15px;


}

</style>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @php if($state!=""): @endphp
        <div class="alert alert-success" role="alert">
            <strong>Confirmation</strong> {{ $msg }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @php endif; @endphp
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Présentation du programme de fidélité du commerce
                </h2>
            </div>
        </div>
        <div class="card-body">
            <div class="text-center">
                <div class="container">
                    <!-- Listing of e-commerce -->
                        <h3>{{ $program->name }}</h3>
                        <!-- replace to 'asset('storage/program/' . $item->user_id . '/' . $item->image . '')' -->
                        <img src="//placehold.it/200" alt="">
                        <div class='bg-white px-5 py-5 mt-4 mx-3'>
                            <p>{{ $program->description }}</p>
                        </div>
                    @php 
                        if(count($fidelity)>0):
                    @endphp
                    <h4>Progression</h4>
                    <br>
                     <div class="container p-0">
                        <div class="row"><br />
                            <div class="col-md-8" style="left: 12%" >
                                <div class="progress">
                                    @foreach ($targetPoints as $target)
                                        <div class="point @php if($fidelity[0]->pointUser > $target->objectifPoint): @endphp primary-color @php else:  @endphp no-color @php endif; @endphp"  
                                        style="left: {{ ($target->objectifPoint / $lastTarget->objectifPoint * 94)}}%" >
                                        <span class="title"><b>{{ $target->objectifPoint }}</b><span>
                                    </div>
                                    @endforeach
                                    <div class="progress-bar" 
                                    style="width: {{ $fidelity[0]->pointUser/$lastTarget->objectifPoint *100  }}%;">{{ $fidelity[0]->pointUser/$lastTarget->objectifPoint *100  }}%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php 
                        else:
                    @endphp
                        <form method="POST" action="{{  route('fidelity.create', $program->program_id)  }}">
                            @csrf
                            <input type="submit" class="btn btn-primary" id="signup" name="signup"  value="Participer au programme de fidelité" >
                        </form>
                    @php    
                        endif;
                    @endphp
                </div>
            </div>
        </div>
    </div>
@endsection
