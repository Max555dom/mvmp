@extends('layouts.app')
@section('content')
<div class="container-fluid">
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <div class="row">
            <div class="col-8">
               <br><br>
               <h2 class="m-0 font-weight-bold text-primary text-center">{{ $townhall[0]->name }}</h2>
            </div>
            <div class="col-4">
               <img width="40%" src="{{ asset('storage/ImagesTownHall/' . $townhall[0]->image . '') }}"
                  alt="img-fluid">
            </div>
         </div>
      </div>
      <div class="card-body">
         <div class="row">
            <div class="col-md-4 text-center">
               <div class="card">
                  <div class="card-header">
                     <?= count($outstanding) ?>
                  </div>
                  <div class="card-body">
                     <h5 class="card-title">COMMERCE(S) EN ATTENT(S)</h5>
                     <br>
                     <button name="outstanding" value="Waiting" id="outstanding state" class="state btn btn-primary">Voir la liste</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="card">
                  <div class="card-header">
                     <?= count($validate) ?>
                  </div>
                  <div class="card-body">
                     <h5 class="card-title">COMMERCE(S) VALIDE(S)</h5>
                     <br>
                     <button name="validate" value="Validate" id="validate" class="state btn btn-primary state">Voir la liste</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4 text-center">
               <div class="card">
                  <div class="card-header">
                     <?= count($unset) ?>
                  </div>
                  <div class="card-body">
                     <h5 class="card-title">COMMERCE(S) DESACTIVE(S)</h5>
                     <br>
                     <button name="unset" value="Disabled" id="validate" class=" state btn btn-primary state">Voir la liste</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <hr>
      <table id="shops" class="display nowrap" width="100%">
      </table>
   </div>
</div>
<style type="text/css">
   /*Hide headers of dataTable*/
   thead {
   position: absolute !important;
   top: -9999px !important;
   left: -9999px !important;
   }
</style>
<script>

    var columnDefs = @json($columns);

    // Call the dataTables jQuery plugin
    $('.state').click(function() {
       var currentState = $(this).val();
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
        //DataTable of shops
        var table = $('#shops').DataTable({
           "destroy": true,
            columns: columnDefs,
            "scrollY":    "680px",
            "scrollCollapse": true,
            'bInfo': false,
            columnDefs: // for display an image
                [
                    {
                        "targets": 0,
                        "data": 'id',
                        "visible": false
                    },
                    {
                        "targets": 1,
                        "data": 'image',
                        "visible": true,
                        "render": function (data, type, row, meta) {
                           let link = '';
                           switch (row['state']) {
                              case 'Waiting':
                                 link = 'commerceValidation';
                                 break;
                              case 'Validate':
                                 link = '';
                                 break;
                              case 'Disabled':
                                 link = '';
                              default:
                              link = '';
                                 break;
                           }
                           let result = "<div class='container'>";
                                 result += "<div class='row'>";
                                    result += "<div class='col-md-2'>";
                                    // to add for fusion => src='./storage/ImagesTownHall/'.row['image']
                                    result += "<img src='./storage/ImagesShop/"+row['image']+ " ' class='img-fluid' alt=''>";
                                    result += "</div>";
                                    result += "<div class='col-md-10'>";
                                       result += row['name'] + "<br>" + " "+ row['address'];
                                       result += "&nbsp;&nbsp;<a href='"+link+"/"+row['id']+"' class='btn btn-primary' >Voir le commerce</a>";
                                    result += "</div>";
                                 result += "</div>";
                              result += "</div><br><br>";
                           return result ;
                        }
                    },
                    {
                        "targets": 2,
                        "data": 'name',
                        "visible": false
                    },
                    {
                        "targets": 3,
                        "data": 'state',
                        "visible": false
                    },
                    {
                        "targets": 4,
                        "data": 'address',
                        "visible": false
                    },
                ],
            'ajax': {
                url: 'api/townHall/shops/'+ currentState ,
                method: 'GET',

            },
            'language' :  {
                'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
            },

        });

        //Rediction of buton of each row
        $('#shops tbody').on( 'click', 'label', function () {
                var data = table.row( $(this).parents('tr') ).data();
                window.location.href = "commerce/" + data['id'];
        });

    });
    </script>
@endsection


