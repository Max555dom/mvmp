<markers>
    @foreach ($shops as $shop)
    <marker 
    id="{{ $shop->id }}"
    name="{{ $shop->name }}"
    description="{{ $shop->description }}"
    address = "{{ $shop->address }}, {{ $shop->zipCode }}, {{ $shop->city }}"
    >
    </marker>
 @endforeach
</markers>