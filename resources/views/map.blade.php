@extends('layouts.app')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Les commerces de ma ville</h2>
            </div>
        </div>
        <div id="map"></div>
        <div class="card-body">
            <div class="table-responsive text-center">
                <div class="container">
                    <div id="position" class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-success">
                            <input type="radio" name="pos" value="All" checked>All
                        </label>
                        <?php foreach ($resultsType ?? '' as $oneType) { ?>
                        <label class="btn btn-success">
                            <input type="radio" name="pos"
                                value="<?php echo $oneType->id; ?>"><?php echo
                            $oneType->name; ?>
                        </label>
                        <?php } ?>
                    </div>
                    <hr>
                    <table id="example" class="display nowrap" width="100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        /*Hide headers of dataTable*/
        thead {
            position: absolute !important;
            top: -9999px !important;
            left: -9999px !important;
        }
    </style>
    <script>
        var columnDefs = @json($Columns);
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            //Display the menu on side-bar
            $('#collapseNavigation').collapse();
            //DataTable of shops 
            var table = $('#example').DataTable({
                columns: columnDefs,
                "scrollY": "680px",
                "scrollCollapse": true,
                'bInfo': false,
                columnDefs:
                    // for displaying an image
                    [{
                            "targets": 1,
                            "data": '//placehold.it/200',
                            "render": function(data, type, row, meta) {
                                return "<img src='" + row[1] + "' class='img-fluid' alt=''>";
                            }
                        },
                        // for displaying name and description of the shop
                        {
                            "targets": 3,
                            "data": 'unknown',
                            "render": function(data, type, row, meta) {
                                return "<td>" + row[2] + "<br>" + row[3] + "</td><br><br>";
                            }
                        },
                        // for displaying the button of google map research
                        {
                            "targets": 7,
                            "data": 'unknown',
                            "render": function(data, type, row, meta) {
                                return "<td><button class='btn btn-success shopLoc'  value='" +
                                    row[6] + ", " + row[7] + ", " + row[8] +
                                    " ' >Voir le commerce</button></td><br><br>";
                            }
                        }
                    ],
                'ajax': {
                    url: '/api/commerces',
                    method: 'GET'
                },
                'language': {
                    'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
                },
            });
            // Filter of dataTable
            $('input:radio').on('change', function() {
                //build a regex filter string with an or(|) condition
                var positions = $('input:radio[name="pos"]:checked').map(function() {
                    return '^' + this.value + '$';
                }).get().join('|');
                if (positions == "^All$") {
                    table.columns().search("").draw();
                } else {
                    table.column(5).search(positions, true, false, false).draw(false);
                }
            });
        });

    </script>
    <script src='./js/mapCommerce.js'></script>
@endsection
