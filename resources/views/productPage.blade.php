@extends('layouts.app')

@section('content')

    <div class="container">
        @if(session()->has('cart'))
            <div class="modal">
                <div class="modal-content center-align">
                    <h5>Produit ajouté au panier avec succès</h5>
                    <hr>
                    <p>Il y a {{ $cartCount }} @if($cartCount  > 1) articles @else article @endif dans votre panier pour un total de <strong>{{ number_format($cartTotal ?? '', 2, ',', ' ') }} € TTC</strong> hors frais de port.</p>
                    <p><em>Vous avez la possibilité de venir chercher vos produits sur place, dans ce cas vous cocherez la case correspondante lors de la confirmation de votre commande et aucun frais de port ne vous sera facturé.</em></p>
                    <div class="modal-footer">
                        <button class="modal-close btn waves-effect waves-light left" id="continue">
                            Continuer mes achats
                        </button>
                        <a href="{{ route('panier.index') }}" class="btn waves-effect waves-light">
                            <i class="material-icons left">check</i>
                            Commander
                        </a>
                    </div>
                </div>

            </div>
        @endif
    </div>


    <div class="container-fluid">
        <div class="shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">{{$product->name}}</h2>
            </div>
        </div>
        <div class="row-cols-md-10">
            <div class="row">
                <div class="col-md-4">
                    {{$product->image}}
                    <img src="//placehold.it/400" class="img-fluid" alt="">
                </div>
                <div class="col-md-8">
                    <label>Description :</label>
                    <p class="card-text">{{$product->description}}</p>
                    <br>

                    <label>Produits restant : {{$product->inventory}}</label>

                    <br>

                    <label>Prix : {{$product->priceTTC}}</label>

                    <?php
                        if($isGooddeal == true){
                            ?>
                            <br>
                            <label>Date début promo : {{$startDate}}</label>
                            <br>
                            <label>Date Fin promo: {{$endDate}}</label>
                            <br>
                        <?php
                        }
                        ?>

                    <br>
                    <div class="align-baseline">

                        <form  method="POST" action="{{ route('panier.store') }}">
                            @csrf
                            <div class="input-field col">
                                <input type="hidden" id="id" name="id" value="{{ $product->id }}">
                                <input id="quantity" name="quantity" type="number" value="1" min="1">
                                <label for="quantity">Quantité</label>
                                <p>
                                    <button class="btn waves-effect waves-light" style="width:100%" type="submit" id="addcart">Ajouter au panier
                                        <i class="fas fa-shopping-cart"></i>
                                    </button>
                                </p>
                            </div>
                        </form>

                        <div class="row">
                            <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5"
                                   data-step="0.1" value="{{ $product->grade }}" data-size="xs" disabled="">
                            <a href="" class="linked ">En voir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="shadow mb-12">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Avis Client</h2>
            </div>
        </div>


        <div class="row">

            @foreach($reviews as $review)

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">


                            <h5 class="card-title">{{$review->title , session('user')}} </h5>
                            <p class="card-text">{{$review->description}}</p>
                            <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5"
                                   data-step="0.1" value="{{ $review->grade }}" data-size="xs" disabled="">
                            <h6 class="container" id="messageChat">@if($review->answer !== null) Reponse
                                : {{$review->answer}} @endif</h6>


                        </div>
                    </div>
                </div>

            @endforeach

            <br>

            <div class="col-md-12">
                <div class="shadow mb-1"></div>


                <form method="post" action="{{route('storeReview' )}}"
                      enctype="multipart/form-data">

                    @csrf
                    <input type="hidden" name="user_id" value="1">
                    <input type="hidden" name="shop_id" value="{{$product->shop_id}}">
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <div class="form-group">
                        <label>Titre</label>
                        <input type="text" class="form-control" name="title" required>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" rows="5" class="form-control"></textarea>
                    </div>

                    <input id="input-1" name="grade" class="rating rating-loading" data-min="0" data-max="5"
                           data-step="0.5" value="" data-size="xs">

                    <button type="submit" class="btn btn-primary mt-5">Ajouter</button>

                </form>

            </div>

        </div>

        <div class="shadow mb-4">
            <h2 class="m-0 font-weight-bold text-primary text-center">Produit similaire</h2>
        </div>


        <div class="row">

            @foreach($products as $product)

                <div class="col-md">
                    <div class="card">

                        <a href="{{ route('detailProduct',$product->id) }}">
                            <img src="//placehold.it/200" class="img-fluid" alt="">
                        </a>

                        <div class="card-body">

                            <p class="text-center">{{$product->name}} </p>
                            <form  method="POST" action="{{ route('panier.store') }}">
                                @csrf
                                <div class="input-field col">
                                    <input type="hidden" id="id" name="id" value="{{ $product->id }}">
                                    <input id="quantity" name="quantity" type="hidden" value="1" min="1">

                                    <p>
                                        <button class="btn waves-effect waves-light" style="width:100%" type="submit" id="addcart">Ajouter au panier
                                            <i class="fas fa-shopping-cart"></i>
                                        </button>
                                    </p>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>

            @endforeach

        </div>
    </div>

    <script type="javascript">
        @if(session()->has('cart'))
        document.addEventListener('DOMContentLoaded', () => {
            const instance = M.Modal.init(document.querySelector('.modal'));
            instance.open();
        });
        @endif
    </script>

@endsection




