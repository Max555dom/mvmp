@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->

<div class="container add">

    <!-- add -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center">Mise a jour du stock d'un article</h2>
        </div>
        <div class="card-body">

            <div class="table text-center">

              <form id='add-form'>
              <div class="form-group col">
                    <img src="{{ asset('storage/ProductsImages/'.$product->image) }}"  id="image" class='img-fluid' alt=''>
                  </div>
                <div class="form-row">

                  <div class="form-group col">
                    <label for="name">Nom du produit</label>
                    <input name= "name" id="name" type="text" class="form-control" value="<?php echo $product->name;?>" placeholder="Nom du produit...." readonly/>
                  </div>
                  

                  <div class="form-group col-md-6">
                  <label for="type">Categorie du produit</label>
                  <input name= "type" id="type" type="text" class="form-control"  value="<?php echo $product->typename;?>" placeholder="Type de produit" readonly/>
                  </div>
                 
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="ref">Reférence</label>
                    <input name= "ref" id="ref" type="text" class="form-control" value="<?php echo $product->reference;?>" placeholder="123456" readonly/>
                  </div>  
                  <div class="form-group col-md-6">
                    <label for="commentaire">Description</label>
                    <textarea id="commentaire" name="commentaire" rows="3" cols="60" maxlength="255" readonly><?php echo $product->description;?></textarea>
                  </div>  

                </div>


                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="ht">Prix HT</label>
                    <input name= "ht" id="ht" type="text" value="<?php echo $product->priceHT;?>" readonly/>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="ttc">Prix TTC</label>
                    <input name= "ttc" id="ttc" type="text" value="<?php echo $product->priceTTC;?>"  readonly/>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="brand">Marque</label>
                    <input name= "brand" id="brand" type="text" value="<?php echo $product->brand;?>" readonly/>
                  </div>  


                </div>
                
                <br>
                <div class="form-row">
                  <div class="form-group col-md-6">
                      <label style="font-size:large;" for="inventory">Stock</label>
                      <input style="font-size:large;" name= "inventory" id="inventory" value="<?php echo $product->inventory;?>" type="text" required/>
                  </div>
                  <div class="form-group col-md-6">
                      <button type='submit' class="btn btn-success">Valider</button>
                  </div>  
                </div>
               
              </form>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->

<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mise a jour d'un article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Succée de la mise a jour du stock de l'article</p>
      </div>
      <div class="modal-footer">
      <form action='/commerceStock/<?php echo $product->shop_id;?>' method="GET">
        <button type="submit" class="btn btn-success" >Close</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mise a jour d'un article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Erreur dans la mise a jour du stock de l'article</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
.add{
    width: 70%;
    
}

label{
  color: black;
}
#inventory{

  color: black;
}

#image {
    max-width: 70%;
    height: auto;
}
</style>
<script>
var id_product = @json($product->id);
$(document).ready(function() {
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
    // Handle form submission.
    var form = document.getElementById('add-form');

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        
        var form_data  = new FormData();

        var inventory = document.getElementById('inventory').value;
        form_data.append("inventory",inventory);
        form_data.append("id_product",id_product);

        $.ajax({
          url: '/api/update',
          type: 'POST',
          data: form_data,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            console.log(data);
            $('#modal-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('#modal-echec').modal('show');
          }
        });
     
    });

});
</script>
@endsection
