@extends('layouts.app')
@section('content')

  <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center"><?php echo $Shop[0]['name'];?></h2>
        </div>
        <div class="card-body">
            <div class="table-responsive text-center">
            
                
            <div class="container">
        
                <div id="position" class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-success">
                                <input type="radio" name="pos" value="All" checked>All 
                            </label> 
                    <?php
                                  
                        foreach($resultsType ?? '' as $oneType)
                        {          
                        ?>
                            <label class="btn btn-success">
                                <input type="radio" name="pos" value="<?php echo $oneType->id;?>"><?php echo $oneType->name;?>
                            </label>
                        <?php 
                        }
                        ?>
                </div>
                    <hr>
                    <table id="example" class="table table-bordered table-striped" width="100%"> 
                    </table>
            </div>

    

    
        </div>
    </div>
</div>
<style type="text/css">


</style>

<script>
var columnDefs = @json($Columns);
var idShop = @json($Shop[0]['id']);
// Call the dataTables jQuery plugin
$(document).ready(function() {


    var table = $('#example').DataTable({
        dom: 'RlBfrtip', 
        select: 'single',
        'lengthMenu': [ 20, 50, 75, 100 ],
        "scrollY":        "680px",
        "scrollCollapse": true,
        columns: columnDefs,
        buttons: [
            {
                text: "Ajouter un nouveau produit",
                action: function (e, dt, node, config) {
                    window.location.href = "/commerceStock/create/" + idShop;
                } 
            },
            {
                extend: 'selected',
                text: "Modifier la fiche du produit",
                action: function (e, dt, node, config) {
                    var id_product = table.row( { selected: true } ).data()[0];
                    window.location.href = "/commerceStock/update/" + id_product;
                } 
            },
        ],
        'ajax': {
            url: '/api/commerce/stock/' + idShop,
            method: 'GET'
        },
        'language' :  {
            'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
        },
    });


    

    // Filter of dataTable
    $('input:radio').on('change', function () {
        //build a regex filter string with an or(|) condition
        var positions = $('input:radio[name="pos"]:checked').map(function() {
            return '^' + this.value + '$';
        }).get().join('|');
        if(positions == "^All$")
        {
            table.columns().search("").draw();
        }else{
            table.column(6).search(positions, true, false, false).draw(false);
        }
    });
 
   
});
</script>
@endsection

