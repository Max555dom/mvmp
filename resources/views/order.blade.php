@extends('layouts.app')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Mes commandes</h2>
            </div>
            <?php
            $idx = 0;
            Foreach ($order as $oneOrder) {
            //dd($oneOrder['order_id']);
            ?>

            <div class="card-body">

                <!-- Section Info général de la commande-->
                <div class=" text-center">
                    <p>Retrouvez ci-dessous la liste des produits que vous avez dans votre commande numéro : <?= $oneOrder['order_id']?> </p>
                    <p> Date de livraison : <?= $oneOrder['order_delivery'] ?> <i class="fas fa-cube"></i><br>
                        Statut de votre commande: <?= $oneOrder['order_state']?><br>
                        Prix de votre commande :  <?= $oneOrder['order_price'] ?> €<br>
                        Boutique : <?= $oneOrder['shop_name'][$idx]?> <br>
                    </p>
                    <br>

                    <!-- Section details produit  -->
                    <div class="row">
                        <?php
                            Foreach($oneOrder['product']  as $product) {
                        ?>
                        <div class="col-md-3">
                            <div class="card" style="min-height: 100%;">
                                <div class="col-auto">
                                    <img src="//<?= $product->image ?>" class="img-fluid" alt="">
                                </div>
                                <h4 class="card-title"> <?= $product->name; ?> </h4>
                                <p class="card-title">  <?=  $product->priceHT; ?> € HT </p>
                                <p class="card-title"> <?= $product->priceTTC; ?> € TTC</p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <hr>
            <?php  }  #$idx = $idx + 1;   ?>

        </div>
    </div>



    <style type="text/css">
        .container{
            width: 900px;
        }
    </style>


@endsection
