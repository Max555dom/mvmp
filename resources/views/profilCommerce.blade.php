@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Profil Commerce</h2>
            </div>

            <!--recup info -->
            <div class="card-body text-center">
                <h2 class=" card-header text-center"
                    style="display: flex; align-items: center; justify-content: center;"> Informations
                    générales </h2>
                <br>
                <div class="form-group">
                    <label>Nom du commerce: {{ $shop['name'] }}</label>
                </div>

                <div class="form-group">
                    <label>Type de commerce: {{ $typeShop['name'] }}</label>
                </div>
                <div class="form-group">
                    <label>Siren: {{$shop['siren'] }}</label>
                </div>
                <div class="form-group">
                    <label>Siret: {{$shop['siret'] }}</label>
                </div>
                <div class="form-group">
                    <label>Adresse: {{ $entity['address'] }} </label>
                </div>
                <div class="form-group">
                    <label>Code postal: {{$entity['zipCode']}}  </label>
                </div>
                <div class="form-group">
                    <label>Ville: {{$entity['city']}} </label>
                </div>

                <h3 style="display: flex; align-items: center; justify-content: center;"> Mode d'achat autorisé </h3>
                <br>
                <div class="form-group">
                    <div class="form-check form-switch">
                        <label class="form-check-label" for="flexSwitchCheckChecked">Click and Collect :   </label>
                        @if($shop['click&collect']==true)
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                               name="clickandcollect" checked>
                        @else
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                                   name="clickandcollect">
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check form-switch">
                        <label class="form-check-label" for="flexSwitchCheckChecked">Click and Collect :   </label>
                        @if($shop['deliveryHome']==true)
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                                   name="deliveryHome" checked>
                        @else
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                                   name="deliveryHome">
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check form-switch">
                        <label class="form-check-label" for="flexSwitchCheckChecked">Click and Collect :   </label>
                        @if($shop['deliveryPoint']==true)
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                                   name="deliveryPoint" checked>
                        @else
                            <input readonly class="form-check-input" type="checkbox" id="flexSwitchCheckChecked"
                                   name="deliveryPoint">
                        @endif
                    </div>
                </div>
            </div>


            <!--Formulaire update info -->
            <h2 class="card-header text-center">Modification du commerce</h2>
            <div class="justify-content-xl-center align-items-center" style="padding: 0% 20%">
                <form enctype="multipart/form-data" method="post" name="updateProfilCommerceForm"
                      action="{{route('updateProfilCommerceForm')}}">
                    @csrf
                    <h3 style="display: flex; align-items: center; justify-content: center;"> Informations
                        Générales </h3>
                    <div class="form-group">
                        <label>Mail: </label>
                        <input name="mail" class="form-control" value="<?php echo $entity['mail'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Numéro de téléphone : </label>
                        <input name="numero" class="form-control" value="<?php echo $entity['numero'];?>" required>
                    </div>

                    <h3 style="display: flex; align-items: center; justify-content: center;"> Informations
                        supplémentaire </h3>
                    <br>
                    <div class="form-group">
                        <label>Introductions de votre commerce </label>
                        <input name="intro" class="form-control" value="<?php echo $shop['intro'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Description de votre commerce </label>
                        <input type="text" name="description" class="form-control"
                               value="<?php echo $shop['description'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Heure d'ouverture du commerce</label>
                        <input name="hours" class="form-control" value="<?php echo $shop['hours'];?>"
                               placeholder="9h00-12h00 14h00-19h00" required>
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" name="logo" id="logo" accept="image/png" value="<?php echo $shop['logo'];?>"
                               class="form-control" required>
                    </div>

                    <h3 style="display: flex; align-items: center; justify-content: center;"> Mode d'achat
                        autorisé </h3>
                    <br>
                    <div class="form-group">
                        <label>Click and Collect </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect1"
                                   value="true">
                            <label class="form-check-label" for="clickAndCollect1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect2"
                                   value="false">
                            <label class="form-check-label" for="clickAndCollect2">Non</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Livraison à domicile : </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome1"
                                   value="true">
                            <label class="form-check-label" for="deliveryHome1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome2"
                                   value="false">
                            <label class="form-check-label" for="deliveryHome2">Non</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Livraison en point relay : </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint1"
                                   value="true">
                            <label class="form-check-label" for="deliveryPoint1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint2"
                                   value="false">
                            <label class="form-check-label" for="deliveryPoint2">Non</label>
                        </div>
                    </div>

                    <h3 style="display: flex; align-items: center; justify-content: center;"> Réseaux sociaux </h3>
                    <br>
                    <div class="form-group">
                        <label>Facebook</label>
                        <input name="FBlink" class="form-control" value="<?php echo $shop['FBlink'];?>">
                    </div>

                    <div class="form-group">
                        <label>Instagram </label>
                        <input name="Instalink" class="form-control" value="<?php echo $shop['Instalink'];?>">
                    </div>
                    <div class="form-group">
                        <label>Youtube </label>
                        <input name="YTlink" class="form-control" value="<?php echo $shop['YTlink'];?>">
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="id" value="<?php echo $shop['id'];?>">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="entity_id" value="<?php echo $shop['entity_id'];?>">
                    </div>
                    <div class="d-grid col-3 mx-auto">
                        <button type="submit" class='btn btn-success btn-lg'> Mettre à jour les informations de mon
                            commerce
                        </button>
                    </div>

                </form>

            </div>
            <br><br><br><br>
        </div>
    </div>


    <style type="text/css">
        .container {
            width: 900px;
        }
    </style>



@endsection
