@extends('layouts.app')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center"><?php echo $Shop[0]['name'];?></h2>
            </div>
            <div class="card-body">
                <div class="table-responsive text-center">
                    <img src="<?php echo $Shop[0]['image'];?>"/>
                    <br>
                    @php if($report): @endphp
                    <button id="reportBtn"  type='button-supp' data-toggle="modal" data-target="#modalReport" class="btn btn-danger pull-right" >Signaler</button>
                    <br>
                    @php endif @endphp
                    <?php if($Shop[0]['intro'] != null){ ?>
                    <p><?php echo $Shop[0]['intro'];?></p>
                    <?php } ?>
                    <hr>
                    <input class="rating rating-loading" data-min="0" data-max="5" data-step="0.1"
                           value="<?php echo $Shop[0]['grade'];?>" data-size="xs" disabled="">
                    <div class="container" style="width:60%;"> <!-- voir les avis user  scrol down the page-->
                        <h4>Description</h4>

                        <hr>
                        <p><?php echo $Shop[0]['description'];?></p> <!--description of all product  -->
                        <a href="<?php echo $Shop[0]['FBlink'];?>">
                            <button type="button" class="btn btn-outline-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor"
                                     class="bi bi-facebook" viewBox="0 0 16 16">
                                    <path
                                        d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                </svg>
                            </button>
                        </a>
                        <a href="<?php echo $Shop[0]['Instalink'];?>">
                            <button type="button" class="btn btn-outline-warning">
                                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor"
                                     class="bi bi-instagram" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
                                </svg>
                            </button>
                        </a>
                        <a href="<?php echo $Shop[0]['YTlink'];?>">
                            <button type="button" class="btn btn-outline-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor"
                                     class="bi bi-youtube" viewBox="0 0 16 16">
                                    <path
                                        d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.122C.002 7.343.01 6.6.064 5.78l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/>
                                </svg>
                            </button>
                        </a>
                    </div>
                    <br>
                </div>
                <div class="container">
                    <hr>
                    <div id="position" class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-success">
                            <input type="radio" name="pos" value="All" checked>All
                        </label>
                        <?php
                        foreach($resultsType ?? '' as $oneType)
                        {
                        ?>
                        <label class="btn btn-success">
                            <input type="radio" name="pos"
                                   value="<?php echo $oneType->id;?>"><?php echo $oneType->name;?>
                        </label>
                        <?php
                        }
                        ?>
                    </div>
                    <hr>
                    <table id="example" class="display nowrap" width="100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal" id='modalReport' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title text-center">Pourquoi signaler-vous ce commerce ?</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <textarea class="form-control" id="reportMsg" placeholder="Votre texte"></textarea>
            </div>
            <div class="modal-footer">
              <form id='del-programme-form'>
                <button type='button' id="report" class="btn btn-success">Valider</button>
                <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" id='modal-success' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Signalement du commerce</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Votre signalement a bien été effectué !</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" id='modalFail' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Signalement du commerce</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Une erreur est survenue. Veuillez réessayer plus tard.</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
            </div>
          </div>
        </div>
      </div>



<style type="text/css">
.container {
    width: 52%;

}

/*Supression des espaces dans la dataTable*/
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 18px;
    border-bottom: 1px solid #111;
}
</style>
<script>
var columnDefs = @json($Columns);
var idShop = @json($Shop[0]['id']);
// Call the dataTables jQuery plugin
$(document).ready(function() {
    var table = $('#example').DataTable({
        columns: columnDefs,
        'bInfo': false,
        "scrollY":        "680px",
        "scrollCollapse": true,
        columnDefs: 
            [
                {
                    "targets": 3,
                    "data": 'unknown',
                    "render": function (data, type, row, meta) {
                        return  "<td>"
                                    + "<div class='card flex-row flex-wrap'>"
                                    + "<div class='card-header border-0'>"
                                    + "<img src='" + row[1] + "' class='card-img-top' alt=''>"
                                    + "</div>"
                                    + " <div class='card-block px-2'>"
                                    + "<br>"
                                    + "<h5 class='card-title'>"
                                    + row[2]
                                    + "</h5>"
                                    + "<p class='card-text'>"
                                    + row[3]
                                    + " €"
                                    + "</p>"
                                    + "<div class='product'>"
                                    + "<form method='post' action='{{ route('panier.store') }}' enctype= 'multipart/form-data'> "
                                    + "<input type='hidden' id='id' name='id' value='"+row[0]+"'>"
                                    + "<input type='hidden' name='_token' value='{{ csrf_token() }}' />"
                                    + "<input  type='hidden' id='quantity' name='quantity' type='number' value='1' min='1'>"
                                    + "<button type='submit' class='btn btn-success mt-5' > Ajouter au panier </button>"
                                    + "</form>"
                            + "</div>"
                            + "<button class='btn btn-primary'>Voir l'article</button>"
                            + "</div>"
                            + "</div>"
                            + "</td>";
                            }
                            },
                            ],
                            'ajax': {
                            url: '/api/commerce/' + idShop,
                            method: 'GET'
                            },
                            'language' :  {
                            'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
                            },
                            });

    // Filter of dataTable
    $('input:radio').on('change', function () {
    //build a regex filter string with an or(|) condition
    var positions = $('input:radio[name="pos"]:checked').map(function() {
    return '^' + this.value + '$';
    }).get().join('|');
    if(positions == "^All$")
    {
        table.columns().search("").draw();
    }else{
        table.column(4).search(positions, true, false, false).draw(false);
    }
    });

    //Rediction Voire article of buton of each row
    $('#example tbody').on( 'click', 'button', function () {
    var data = table.row( $(this).parents('tr') ).data();
    window.location.href = "/product/" + data[0]; // chemin verst
    });
});
    </script>
    <script>
        $('#report').click(function (){
            //console.log('step1 ok');
            let idShop = @json($Shop[0]['id']);
            let msg = $('#reportMsg').val();
            let form_data  = new FormData();
            form_data.append("msg", msg);
            // console.log(msg);
            $.ajax({
                url: '/api/report/shop/'+idShop,
                type: 'post',
                data: form_data,
                dataType:'json',
                contentType: false,
                processData: false,
                success: function() {
                    $('#reportBtn').remove();
                    $('#modalReport').modal('hide');
                    $('#modal-success').modal('show');

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#modalFail').modal('show');
                }
        });
        });
    </script>
@endsection

