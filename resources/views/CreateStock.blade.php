@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->

<div class="container add">

    <!-- add -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center">Création d'un article</h2>
        </div>
        <div class="card-body">

            <div class="table text-center">

              <form id='add-form'>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="name">Nom du produit</label>
                    <input name= "name" id="name" type="text" class="form-control"  placeholder="Nom du produit...." required/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="file">Image</label>            
                    <input name="file[]" id="files" type="file" multiple required>
                    <!--<button class="btn btn-success btn-upload">Apperçu</button>-->
                  </div>

                  <div class="form-group col-md-6">
                  <label for="type">Categorie du produit</label>
                  <input name= "type" id="type" type="text" class="form-control"  placeholder="Type de produit" required/>
                  </div>
                  <!--
                  <div class="form-group col-md-6">
                    <img src="image_<?php// echo $id_Shop;?>" class='img-fluid' alt=''>
                  </div>
                  -->
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="ref">Reférence</label>
                    <input name= "ref" id="ref" type="text" class="form-control" placeholder="123456" required/>
                  </div>    
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="commentaire">Description</label>
                    <textarea id="commentaire" name="commentaire" rows="3" cols="130" maxlength="255" required></textarea>
                  </div>    
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="ht">Prix HT</label>
                    <input name= "ht" id="ht" type="text" required/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="ttc">Prix TTC</label>
                    <input name= "ttc" id="ttc" type="text"  required/>
                  </div>   
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="brand">Marque</label>
                    <input name= "brand" id="brand" type="text"  required/>
                  </div>  
                  <div class="form-group col-md-6">
                    <label for="inventory">Stock</label>
                    <input name= "inventory" id="inventory"  type="text" required/>
                  </div>    
                </div>
                <button type='submit' class="btn btn-success">Valider</button>
              </form>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->

<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ajout d'un article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre ajout à bien était effectué</p>
      </div>
      <div class="modal-footer">
      <form action='/commerceStock/<?php echo $id_Shop;?>' method="GET">
        <button type="submit" class="btn btn-success" >Close</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Echec de l'ajout d'un article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Erreur dans l'ajout de l'article</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
.add{
    width: 70%;
    
}

label{
  color: black;
}

</style>
<script>
var idShop = @json($id_Shop);
$(document).ready(function() {
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
    // Handle form submission.
    var form = document.getElementById('add-form');

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        
        var form_data  = new FormData();

        var name = document.getElementById('name').value;
        form_data.append("name",name);

      

        var totalfiles = document.getElementById('files').files.length;
        // Read selected files
        for (var index = 0; index < totalfiles; index++) {
            var file = document.getElementById('files').files[index];
            var name = file.name;
            form_data.append("files[]", file, name);
        }




        var type = document.getElementById('type').value;
        form_data.append("type",type);

        var ref = document.getElementById('ref').value;
        form_data.append("ref",ref);
        
        var commentaire = document.getElementById('commentaire').value;
        form_data.append("commentaire",commentaire);


        var ht = document.getElementById('ht').value;
        form_data.append("ht",ht);

        var ttc = document.getElementById('ttc').value;
        form_data.append("ttc",ttc);

        var brand = document.getElementById('brand').value;
        form_data.append("brand",brand);

        var inventory = document.getElementById('inventory').value;
        form_data.append("inventory",inventory);

        form_data.append("idShop",idShop);

        $.ajax({
          url: '/api/add',
          type: 'POST',
          data: form_data,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            console.log(data);
            $('#modal-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('#modal-echec').modal('show');
          }
        });
     
    });


/*
    $("#btn-upload").click(function(){
    var form_data  = new FormData();
    var file = document.getElementById('file').file;
    var name = file.name;
    form_data.append("file", file, name);
    
    $.ajax({
        url: '/api/image-upload',
        type: 'POST',
        data: form_data,
        dataType:'json',
        contentType: false,
        processData: false,
        success: function(data) {
         
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error on Upload Request');
        }
    });
  });
  */

});
</script>
@endsection
