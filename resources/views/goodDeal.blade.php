@extends('layouts.app')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Les Bons Plans</h2>
            </div>
            <div class="card-body">

                <div class="table-responsive text-center">

                    <div class="container">

                        <!-- CAROUSEL -->
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $cpt=0?>
                                @foreach($carousel as $oneCarousel)
                                    @if($cpt==0)
                                    <div class="carousel-item active">
                                        <a href="{{ route('getDetailProduitGoodDeal', $oneCarousel->id) }} " >
                                            <img  id='redirect' src="{{ asset('storage/ImagesGoodDeal/'.$oneCarousel->image) }}" alt="{{$oneCarousel->name}}" class="image-fluid" />
                                        </a>
                                    </div>
                                            <?php $cpt= $cpt+1 ?>
                                    @else
                                        <div class="carousel-item">
                                            <a href="{{ route('getDetailProduitGoodDeal', $oneCarousel->id) }} " >
                                                <img  id='redirect' src="{{ asset('storage/ImagesGoodDeal/'.$oneCarousel->image) }}" alt="{{$oneCarousel->name}}" class="image-fluid" />
                                            </a>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next" >
                                <span class="carousel-control-next-icon"  aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                        <!-- DATA TABLE-->
                        <hr>
                        <div id="position" class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-success">
                                <input type="radio" name="pos" value="All" checked>All
                            </label>
                            <?php
                            foreach($resultsType ?? '' as $oneType)
                            {
                            ?>
                            <label class="btn btn-success">
                                <input type="radio" name="pos"
                                       value="<?php echo $oneType->id;?>"><?php echo $oneType->name;?>
                            </label>
                            <?php
                            }
                            ?>
                        </div>
                        <hr>

                        <table id="gooddeal" class="display nowrap" width="100%">
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <style type="text/css">

            .carousel-control-prev-icon{
                background: blue;
            }
            .carousel-control-next-icon {
                background: blue;
            }
            /*Hide headers of dataTable*/
            thead {
                position: absolute !important;
                top: -9999px !important;
                left: -9999px !important;

            }

            thead > th {
                width: 20px !important;
            }

        </style>
        <script>

            var columnDefs = @json($Columns);
            // Call the dataTables jQuery plugin
            $(document).ready(function () {
                //Display the menu on side-bar
                $('#collapseNavigation').collapse();

                //DataTable of shops
                var table = $('#gooddeal').DataTable({
                    columns: columnDefs,
                    "scrollY": "680px",
                    "scrollCollapse": true,
                    'bInfo': false,
                    columnDefs: // for display an image
                        [
                            {
                                "targets": 3,
                                "data": 'unknown',
                                "render": function (data, type, row, meta) {
                                    return "<td>"
                                        + "<div class='card flex-row' style='text-align: left'>"
                                        + "<div class='card-header border-0'>"
                                        + "<img src='" + row[1] + "' class='card-img-top' alt=''>"
                                        + "</div>"
                                        + " <div class='card-block px-2'>"
                                        + "<br>"
                                        + "<h4 class='card-title'>"
                                        + row[2]
                                        + "</h4>"
                                        + row[10]
                                        + "<h3>"
                                        + "Prix : "
                                        + row[7]
                                        + " € "
                                        + "</h3>"
                                        + "<div class='produit'>"
                                        + "<form method='post' action='{{ route('panier.store') }}' enctype= 'multipart/form-data'> "
                                        + "<input type='hidden' id='id' name='id' value='" + row[0] + "'>"
                                        + "<input type='hidden' name='_token' value='{{ csrf_token() }}' />"
                                        + "<input  type='hidden' id='quantity' name='quantity' type='number' value='1' min='1'>"
                                        + "<button type='submit' class='btn btn-success mt-5' > Ajouter au panier </button>"
                                        + "</form>"
                                        +"<form method='post' action='{{ route('detailProduitGoodDeal') }}' enctype= 'multipart/form-data'> "
                                        + "<input type='hidden' id='id' name='id' value='" + row[9] + "'>"
                                        + "<input type='hidden' name='_token' value='{{ csrf_token() }}' />"
                                        + "<input type='hidden' name='price' value='" + row[7] + "'/>"
                                        + "<input type='hidden' name='startDate' value='" + row[4] + "'/>"
                                        + "<input type='hidden' name='endDate' value='" + row[5] + "'/>"
                                        + "<button type='submit' class='btn btn-primary mt-5' > Voir le produit </button>"
                                        + "</form>"
                                        + "</div>"
                                        + "</div>"
                                        + "</div>"
                                        + "</td>";
                                }
                            },
                            {
                                "targets": 5,
                                "data": 'unknown',
                                "render": function (data, type, row, meta) {
                                    return "<td>"
                                        + "<div class='card flex-row' style='text-align: center'>"
                                            + " <div class='card-block px-2'>"
                                                + "<br>"
                                                + "<h3 class='card-title'> Discount : - "
                                                    + row[6]
                                                    + " % "
                                                + "</h3>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<br/>"
                                                + "<p>"
                                                    + "Du  "
                                                    + row[4]
                                                    + " au  "
                                                    + row[5]
                                                + "</p>"
                                        + "</div>"
                                        + "</div>"
                                        + "</td>";
                                }
                            }
                        ],
                    'ajax': {
                        url: '/api/listeBonPlan',
                        method: 'GET'
                    },
                    'language': {
                        'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
                    },
                    responsive: true,
                    stateSave: true,
                    success: function (data) {
                        console.log(data);
                        return success;
                    },
                    error: function (data, jqXHR, textStatus, errorThrown) {
                        console.log(data);
                    }
                });

                // Filter of dataTable
                $('input:radio').on('change', function () {
                    //build a regex filter string with an or(|) condition
                    var positions = $('input:radio[name="pos"]:checked').map(function () {
                        return '^' + this.value + '$';
                    }).get().join('|');
                    if (positions == "^All$") {
                        table.columns().search("").draw();
                    } else {
                        table.column(8).search(positions, true, false, false).draw(false);
                    }
                });

            });
        </script>
@endsection
