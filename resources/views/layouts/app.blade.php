<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Custom fonts for this template-->

    <link href="{{ asset('dist/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/select.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/responsive.dataTables.min.css') }}" rel="stylesheet">


    <!-- Template for star rating-->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>


    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map{
        margin-left:auto;
        margin-right:auto;
        height: 300px;
        width: 80%
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
</head>
<body>

<!-- Page Wrapper -->
<div id="wrapper">

 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MVMP</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-home"></i>
            <span>Accueil</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">



    <!-- Nav Item - Facturation Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="index.html#" data-toggle="collapse" data-target="#collapseNavigation" aria-expanded="true" aria-controls="collapseNavigation">
        <i class="fas fa-store"></i>
        <span>Navigation</span>
        </a>
        <div id="collapseNavigation" class="collapse" aria-labelledby="headingFacturation" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
           <!--  <h6 class="collapse-header">Options de navigation</h6>-->
            <a class="collapse-item" href="{{ route('accueil-commerces') }}">Commerces</a>
            <a class="collapse-item" href="{{ route('paiment') }}">Paiments</a>
            <a class="collapse-item" href="/commerceValidation/<?php echo 1;?>">Validation du commerce</a>
            <a class="collapse-item" href="/commerceStock/<?php echo 1;?>">Gestion de stock du commerce</a>
            <a class="collapse-item" href="/programeFidelity/<?php echo 1;?>">Programme de fildelité</a>
        </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-map-marker"></i>
            <span>Ville</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Utilities:</h6>
                <a class="collapse-item" href="utilities-color.html">Colors</a>
                <a class="collapse-item" href="utilities-border.html">Borders</a>
                <a class="collapse-item" href="utilities-animation.html">Animations</a>
                <a class="collapse-item" href="utilities-other.html">Other</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('townHall') }}" aria-expanded="true">
            <i class="fas fa-home-lg"></i>
            <span>Accueil</span>
        </a>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ route('loyaltyProgram', 2) }}"
           aria-expanded="true" >
          <i class="fas fa-thumbs-up"></i>
          <span>Fidélité commerce (exemple : 2)</span>
      </a>
  </li>

       <!-- Nav Item - Utilities Collapse Menu -->
     <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('listProductsShop') }}"
             aria-expanded="true" >
            <i class="fas fa-thumbs-up"></i>
            <span>Bons plans</span>
        </a>
    </li>

     <li class="nav-item">
         <!-- Todo changer le 1 en id du user quand connection faite -->
         <a class="nav-link collapsed" href="{{ route('orderList',1) }}"
            aria-expanded="true" >
             <i class="fas fa-shopping-cart"></i>
             <span>Commandes</span>
         </a>
     </li>


     <!-- Navabar button for product -->
     <li class="nav-item">
         <a class="nav-link collapsed" href="{{ route('detailProduct',1) }}"
            aria-expanded="true" >
             <i class="fas fa-shopping-cart"></i>
             <span>Product</span>
         </a>
     </li>

       <!-- Navabar button for products -->
       <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('listProductsShop') }}"
           aria-expanded="true" >
            <i class="fas fa-shopping-cart"></i>
            <span>Produits</span>
        </a>
    </li>



     <li class="nav-item">
         <a class="nav-link collapsed" href="{{ route('inscriptionCommerce') }}"
            aria-expanded="true" >
             <i class="far fa-user"></i>
             <span>InscriptionCommerce</span>
         </a>
     </li>

     <li class="nav-item">
         <a class="nav-link collapsed" href="{{ route('waitingValue',4) }}"
            aria-expanded="true" >
             <i class="far fa-user"></i>
             <span>InscriptionCommerceWaiting</span>
         </a>
     </li>

     <li class="nav-item">
         <a class="nav-link collapsed" href="{{ route('profilCommerce',4) }}"
            aria-expanded="true" >
             <i class="far fa-user"></i>
             <span>ProfilCommerce</span>
         </a>
     </li>

     <li class="nav-item">
     <a class="nav-link collapsed" href="{{ route('goodDeal') }}"
        aria-expanded="true" >
         <i class="fas fa-tag"></i>
         <span>Bon plans</span>
     </a>
     </li>




    <!-- Navabar button for shopping Basket -->

     @if($cartCount)
         <li>
             <a class="nav-link collapsed" id="shoppingcart" href="{{ route('panier.index') }}" data-position="bottom" data-tooltip="Voir mon panier">
                 <i class="fas fa-shopping-cart"></i>
                 <span>Panier({{ $cartCount }})</span>

             </a>
         </li>
    @endif



         <!-- Nav Item - Utilities Collapse Menu -->
     <li class="nav-item">
        <a class="nav-link collapsed" href="/map" >
            <i class="fas fa-map"></i>
            <span>Carte</span>
        </a>
    </li>
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>


</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">
    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>

      <!-- Topbar Search -->
      <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
        <div class="input-group">
          <input type="text" class="form-control bg-light border-0 small" placeholder="Rechercher pour..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search fa-sm"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="index.html#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="index.html#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            <span class="badge badge-danger badge-counter">3+</span>
          </a>
          <!-- Dropdown - Alerts -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
              Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="mr-3">
                <div class="icon-circle bg-primary">
                  <i class="fas fa-file-alt text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 12, 2019</div>
                <span class="font-weight-bold">A new monthly report is ready to download!</span>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="mr-3">
                <div class="icon-circle bg-success">
                  <i class="fas fa-donate text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 7, 2019</div>
                $290.29 has been deposited into your account!
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="mr-3">
                <div class="icon-circle bg-warning">
                  <i class="fas fa-exclamation-triangle text-white"></i>
                </div>
              </div>
              <div>
                <div class="small text-gray-500">December 2, 2019</div>
                Spending Alert: We've noticed unusually high spending for your account.
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="index.html#">Show All Alerts</a>
          </div>
        </li>

        <!-- Nav Item - Messages -->
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="index.html#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <!-- Counter - Messages -->
            <span class="badge badge-danger badge-counter">7</span>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
              Message Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div class="font-weight-bold">
                <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                <div class="small text-gray-500">Emily Fowler · 58m</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                <div class="status-indicator"></div>
              </div>
              <div>
                <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                <div class="small text-gray-500">Jae Chun · 1d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                <div class="status-indicator bg-warning"></div>
              </div>
              <div>
                <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                <div class="small text-gray-500">Morgan Alvarez · 2d</div>
              </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="index.html#">
              <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                <div class="status-indicator bg-success"></div>
              </div>
              <div>
                <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                <div class="small text-gray-500">Chicken the Dog · 2w</div>
              </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="index.html#">Read More Messages</a>
          </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="index.html#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small">@guest  @else{{ Auth::user()->email }} @endguest</span>
            <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
          </a>
          <!-- Dropdown - User Information -->

          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
           @guest
                  <a class="dropdown-item" href="{{route('login')}}">
                    <i class="fas fa-sign-in-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    {{ __('Login') }}
                  </a>
                  @if (Route::has('register'))
                      <a class="dropdown-item" href="{{route('register')}}">
                        <i class="fas fa-sign-in-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        {{ __('Register') }}
                      </a>
                  @endif


          @else


                  <a class="dropdown-item" href="index.html#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                     Profile
                  </a>
                <a class="dropdown-item" href="index.html#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="index.html#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.html#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
           @endguest

          </div>
        </li>


      </ul>

    </nav>

  <!------------------------------------------------------------------------->


   <!-- Bootstrap core JavaScript-->
    <!--
   <script src="dist/jquery/jquery.min.js"></script>
    <script src="dist/bootstrap/js/bootstrap.bundle.min.js"></script>
-->
    <!-- Core plugin JavaScript-->
     <!-- <script src="dist/jquery-easing/jquery.easing.min.js"></script>-->

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
   <!-- Core plugin JavaScript-->
   <script src="{{ asset('js/jquery/jquery.easing.min.js') }}"></script>
  <script src="/dist/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>-->


    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins for datatable-->
  <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Datatable C.R.U.D pluging -->
  <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>

  <!-- Datatable Resizing pluging -->
  <script src="{{ asset('js/datatables/ColReorderWithResize.js') }}"></script>

  <script async
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvJhPdZRy8Jg3nimj-QCjcGNU9JEuAp7A&callback=initMap">
</script>

    <!-- End of Topbar -->
    <main class="py-4">
            @yield('content')

    </main>
   <!-- Footer -->
   <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright MVMP &copy; MVMP 2020-2021</span>
          </div>
        </div>
    </footer>
    <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->


      </div>
   <!-- End of Main Content -->
    <!-- Scroll to Top Button-->
    <!--
  <a class="scroll-to-top rounded" href="#page-top">  ////////////////// A fixer
    <i class="fas fa-angle-up"></i>
  </a>
-->
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to go ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Deconnexion" if you want to close that sessions.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          <form  action="{{route('logout')}}" method="POST">
            @csrf
            <button class="btn btn-primary" type="submit" >Deconnexion</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
