@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->

<div class="container add">

    <!-- add -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center">Création d'un programme de fidélité</h2>
        </div>
        <div class="card-body">

            <div class="table text-center">

              <form id='add-form'>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="name">Nom du programme</label>
                    <input name= "name_programme" id="name_programme" type="text" class="form-control"  placeholder="Nom du programme...." required/>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="file">Image</label>            
                    <input name="file[]" id="files" type="file" multiple required>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pointsByCommande">Points attribués par commande</label>
                    <input name= "pointsByCommande" id="pointsByCommande" type="text" class="form-control"  placeholder="5" required/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="minPriceByCommande">Prix minimum de la commande</label>
                    <input name="minPriceByCommande" id="minPriceByCommande" type="text" class="form-control"  placeholder="0" required/>
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group ">
                    <label for="commentaire">Description</label>
                    <textarea id="description" name="description" rows="3" cols="130" maxlength="255" required></textarea>
                  </div> 
                    
                </div>
                <button type='submit' class="btn btn-success">Valider</button> 
              </form>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->

<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ajout du programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre ajout à bien était effectué</p>
      </div>
      <div class="modal-footer">
      <form action='/programeFidelity/<?php echo $id_Shop;?>' method="GET">
        <button type="submit" class="btn btn-success" >Close</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Echec de l'ajout du programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Erreur dans l'ajout du programme de fidélité, veuillez vous assurez de la validité des champs saisits</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
.add{
    width: 70%;
    
}

label{
  color: black;
}

</style>
<script>
var idShop = @json($id_Shop);
$(document).ready(function() {
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
    // Handle form submission.
    var form = document.getElementById('add-form');

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        
        var form_data  = new FormData();

        var name_programme = document.getElementById('name_programme').value;
        form_data.append("name_programme",name_programme);

      

        var totalfiles = document.getElementById('files').files.length;
        // Read selected files
        for (var index = 0; index < totalfiles; index++) {
            var file = document.getElementById('files').files[index];
            var name = file.name;
            form_data.append("files[]", file, name);
        }
        
        var description = document.getElementById('description').value;
        form_data.append("description",description);


        var pointsByCommande = document.getElementById('pointsByCommande').value;
        form_data.append("pointsByCommande",pointsByCommande);

        var minPriceByCommande = document.getElementById('minPriceByCommande').value;
        form_data.append("minPriceByCommande",minPriceByCommande);

        form_data.append("idShop",idShop);

        $.ajax({
          url: '/api/add/programme',
          type: 'POST',
          data: form_data,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {      
            $('#modal-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('#modal-echec').modal('show');
          }
        });
     
    });


/*
    $("#btn-upload").click(function(){
    var form_data  = new FormData();
    var file = document.getElementById('file').file;
    var name = file.name;
    form_data.append("file", file, name);
    
    $.ajax({
        url: '/api/image-upload',
        type: 'POST',
        data: form_data,
        dataType:'json',
        contentType: false,
        processData: false,
        success: function(data) {
         
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error on Upload Request');
        }
    });
  });
  */

});
</script>
@endsection
