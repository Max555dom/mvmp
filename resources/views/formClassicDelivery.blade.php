@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h3 class="h3 mb-0 text-gray-600"> <B><a style="color:blue;" >Panier</a></B> > <B style="color:gray;">Choix de l'adresse de livraison </B></h3>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Formulaire de livraison Classique</h2>
            </div>
            <br>
            <!-- API MONDIAL RELAY - Attention il faut recup l'id de l'order -->
            <h2 class="text-primary text-center">Sélectionner un point relais</h2>

            <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

            <!-- Librairie de Cartographie : Leaflet. -->
            <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
            <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet/dist/leaflet.css"/>

            <!-- Widget MR -->
            <script
                src="https://widget.mondialrelay.com/parcelshop-picker/jquery.plugin.mondialrelay.parcelshoppicker.min.js">
            </script>

            <!-- Zone dans laquelle le Widget est chargé -->
            <div id="Zone_Widget"></div>
            <div class="row d-flex justify-content-center align-items-center">
                <!-- formulaire hidden pour le point relay-->
                <form method="post" name="relaypointForm" action="{{route('addDeliveryAddress')}}">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" name="user_id" class="form-control" value="1">
                        <!-- type="hidden" remplacer le contenue de value par value placeholder on aura les info de la commande-->
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="deliveryMode" class="form-control" value="2">
                        <!-- d'après la table catéhorie la livraison en point relay = 2-->
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="deliveryAddress" name="deliveryAddress" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="city" name="city" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="zipCode" name="zipCode" class="form-control">
                    </div>
                    <button type="submit" class='btn btn-success'>Valider mon point relay et procéder au
                        paiement
                    </button>
                </form>

            </div>
            <br><br>
            <h2 class="text-primary text-center">Ou ajouter une adresse postal</h2>
            <br><br>
            <!-- formulaire pour l'adresse postal de livraison  -->
            <div class="row d-flex justify-content-center align-items-center">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title text-center">Veuillez renseigner les éléments ci-dessous :</p>
                        <form method="post" name="deliveryForm" action="{{route('addDeliveryAddress')}}">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="user_id" class="form-control" value="1">
                                <!--  par value placeholder on aura les info de la commande-->
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="deliveryMode" class="form-control" value="3">
                                <!-- d'après la table catéhorie la livraison a domicile = 3 -->
                            </div>
                            <div class="form-group">
                                <label>Adresse postal: </label>
                                <input name="deliveryAddress" class="form-control" placeholder="12 rue haute" required>
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Ville: </label>
                                <input name="city" class="form-control" placeholder="Paris" required>
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Code postal:</label>
                                <input name="zipCode" class="form-control" placeholder="75001" required>
                                <br>
                            </div>
                            <button type="submit" class='btn btn-success'>Valider mon adresse et procéder au paiement
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <br><br><br><br>
        </div>


        <style type="text/css">
            .container {
                width: 900px;
            }
        </style>

        <script>
            // Initialiser le widget après le chargement complet de la page
            $(document).ready(function () {
                // Charge le widget dans la DIV d'id "Zone_Widget" avec les paramètres indiqués
                $("#Zone_Widget").MR_ParcelShopPicker({
                    // Paramétrage de la liaison avec la page.
                    // Selecteur de l'élément dans lequel est envoyé l'ID du Point Relais (ex: input hidden)
                    Target: "#Target_Widget",
                    // Selecteur de l'élément dans lequel est envoyé l'ID du Point Relais pour affichage
                    TargetDisplay: "#TargetDisplay_Widget",
                    // Selecteur de l'élément dans lequel sont envoysé les coordonnées complètes du point relais
                    TargetDisplayInfoPR: "#TargetDisplayInfoPR_Widget",
                    // Paramétrage du widget pour obtention des point relais.
                    // Le code client Mondial Relay, sur 8 caractères (ajouter des espaces à droite)
                    // BDTEST est utilisé pour les tests => un message d'avertissement apparaît
                    Brand: "BDTEST  ",
                    // Pays utilisé pour la recherche: code ISO 2 lettres.
                    Country: "FR",
                    // Code postal pour lancer une recherche par défaut
                    PostCode: "78760",
                    // Mode de livraison (Standard [24R], XL [24L], XXL [24X], Drive [DRI])
                    ColLivMod: "24R",
                    // Nombre de Point Relais à afficher
                    NbResults: "5",
                    Responsive: true,
                    // Afficher les résultats sur une carte?
                    ShowResultsOnMap: true,
                    // Afficher les informations du point relais à la sélection sur la carte?
                    DisplayMapInfo: true,
                    // Fonction de callback déclenché lors de la selection d'un Point Relais
                    OnParcelShopSelected:
                    // Fonction de traitement à la sélection du point relais.
                    // Remplace les données de cette page par le contenu de la variable data.
                    // data: les informations du Point Relais
                        function (data) {
                            $("#deliveryAddress").val(data.Adresse1);
                            $("#zipCode").val(data.CP);
                            $("#city").val(data.Ville);
                        },
                });

            });
        </script>

@endsection
