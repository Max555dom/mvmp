@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h3 class="h3 mb-0 text-gray-600"><B style="color:blue;">Panier</B> > <B><a style="color:blue;" href='/formClassicDelivery'>Choix de l'adresse de livraison</a></B> > <B style="color:gray;">Paiment de votre commande</B></h3>
  </div>
</div>
<br>
<div class='row'>
    <div class="container-fluid paiment">

        <!-- Paiment -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Paiement</h2>
            </div>
            <div class="card-body">

                <div class="table-responsive text-center">


                    <script src="https://js.stripe.com/v3/"></script>

                    <!-- <button id='checkout-button'>Paiment</button> -->

                    <form id="payment-form">
                        <div id="card-element">
                            <!-- Elements will create input elements here -->
                        </div>
                        <input id="tabId" name="tabId" type="hidden" value="<?php echo rawurlencode(serialize($resultsOrder));?>">
                        <input id="orders" name="orders" type="hidden" value="<?= rawurlencode(serialize($order)); ?>">
                        <!-- discount by order  -->
                        <?php

                        ?>
                        <!-- We'll put the error messages in this element -->
                        <div id="card-errors" role="alert"></div>

                        <button class="btn btn-success btn-paiment" id="submit">Paiment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid recap">

        <!-- Récapitulatif de la commande -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h4 class="m-0 font-weight-bold text-primary text-center">Résumer de ma commande</h4>
            </div>
            <div class="card-body">

                <div class="table-responsive text-center">

                    <div class="card-block px-0">
                        <p class="card-text" >Discount inclut (fidelité) : {{ $discount }} €</p>
                        <hr/>
                        <p class="card-text">Prix HT : <?php echo $totalpriceHT;?> € </p>
                        <hr/>
                        <p class="card-text">Taxes : <?php echo $totalpriceTTC - $totalpriceHT;?>€ </p>
                        <hr/>
                        <p class="card-text">Prix TTC : <?php echo $totalpriceTTC;?> € </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <!-- Begin Listing des articles présent dans la commande -->
    <div class="container-fluid commande">
        <div class="card shadow">
            <div class="card-body">
                <div class="card-header text-center py-2">
                    <h3 class="m-0 text-primary">Détail de ma commande</h3>
                </div>
                <div class="text-center">
                        <?php

                        Foreach ($order as $oneOrder) {

                        ?>
                            <div class="card-header text-center py-1">
                                <br>
                                <h5 ><?= $oneOrder['shop_name'][0]?> </h5>
                                <p><?= $oneOrder['order_price']?> €</p>

                            </div>
                            <!-- Section details produit-->
                            <div class="row">

                                <?php
                                    Foreach($oneOrder['product']  as $product) {
                                ?>
                                <div class="col-md-4">
                                    <div class="card" style="max-height: 100%;min-height:100%;">
                                        <div class="col-auto">


                                            <div class="card-header border-0">
                                                <img src="//<?= $product->image ?>" class="img-fluid" alt="">
                                            </div>
                                            <div class="card-block px-0">
                                                <p class="card-title"><?= $product->name; ?><p>
                                                <p class="card-text">Prix HT : <?= $product->priceHT; ?> € </p>
                                                <p class="card-text">Prix TTC : <?= $product->priceTTC; ?> € </p>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <?php } ?>
                            </div>
                    <?php } ?>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal2 fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Votre paiment a bien était effectué</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Succès du paiment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre paiment de <?php echo $totalpriceTTC ;?> € a bien était effectué</p>
      </div>
      <div class="modal-footer">
      <form action='/' method="GET">
        <button type="submit" class="btn btn-success" >Close</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Echec du paiement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre paiment de <?php echo $totalpriceTTC ;?> € à échoué, veuillez verifié que l'intégralité des informations de paiment sont renseignées</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


        <?php

           // var_dump($order);



                /**
                 *
                 * Create a paiment
                 *
                  */
                  /*
                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                      'name' => 'Kavholm rental',
                      'amount' => 1000,
                      'currency' => 'eur',
                      'quantity' => 1,
                    ]],
                    'payment_intent_data' => [
                      'application_fee_amount' => 123,
                      'transfer_data' => [
                        'destination' => 'acct_1IjnlcC3PfR7BxYJ',
                      ],
                    ],
                    'mode' => 'payment',
                    'success_url' => 'http://127.0.0.1:8000/paiment', // url of success of the paiment
                    'cancel_url' => 'http://127.0.0.1:8000/', // url if the paiment is cancel ( view panier)
                  ]);
                  */
                /*
                *
                * Created an account link
                * la réponse contient une clé url , redirection de l'utilisateur vers l'url
                *
                 $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1Ijme4IpJswWHGyM',
                    'refresh_url' => 'https://example.com/reauth', // rediriger vers l'authentification en cas de refresh
                    'return_url' => 'https://example.com/return', // rediriger vers l'accueil de mvmp  apres la redirection de l'utilisateur vers la clé url retournée
                    'type' => 'account_onboarding',
                  ]);

                  $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1Ijmi4AANTn7XCpp',
                    'refresh_url' => 'https://example.com/reauth',
                    'return_url' => 'https://example.com/return',
                    'type' => 'account_onboarding',
                  ]);

                  $account_links = \Stripe\AccountLink::create([
                    'account' => 'acct_1IjmkvFrpDmvwf1U',
                    'refresh_url' => 'https://example.com/reauth',
                    'return_url' => 'https://example.com/return',
                    'type' => 'account_onboarding',
                  ]);

                    */



                /*
                *   compte = compte stripe
                *   Création du compte connectés au compte principal de MVMP
                *
                $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce1@gmail.com',
                    'type' => 'standard',
                  ]);

                 $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce2@gmail.com',
                    'type' => 'standard',
                  ]);

                 $account = \Stripe\Account::create([
                    'country' => 'FR',
                    'email' => 'commerce3@gmail.com',
                    'type' => 'standard',
                  ]);
                */

    ?>

<style type="text/css">
.paiment{
    width: 40%;
    margin-left: 10%;
}
.recap{
    width: 40%;

}
.commande{
    width: 40%;
    margin-left: 10%;
}


</style>
<script>

$(document).ready(function() {
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
    var stripe = Stripe('pk_test_51Ihv5aDIqoOKjau7MLcUPDfcUg1cTcCsVVm9O4bMG2PlIiQHDitGquTFYPCLec9SJZzfjPFYtoc4SzdX02hNEKC400AQPgQY56'); // public for js

    // Set your publishable key: remember to change this to your live publishable key in production
    // See your keys here: https://dashboard.stripe.com/apikeys
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
    base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
        color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
    };


   // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the card-element <div>.
    card.mount('#card-element');
    // Handle real-time validation errors from the card Element.
    card.on('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        //4000 0000 0000 3220 Faux rib viable pour tester le paiment
      if($('#card-element').hasClass('StripeElement--complete') == true){ // check the bank info with the stripe checker of iban and field
        var tabId = document.getElementById('tabId').value;
        // orders
        var orders = document.getElementById('orders').value;
        $.ajax({
        url: '/api/validateCommande',
        type: 'POST',
        data: {
          "tabId":tabId,
          "orders" : orders
        },
        processData: true,
        success: function(data) {
          $('#modal-success').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $('#modal-echec').modal('show');
        }
    });
      }else{
        $('#modal-echec').modal('show');
      }
    });

   /*
    var checkoutButton = document.getElementById('checkout-button');

    checkoutButton.addEventListener('click', function() {

    // Call your backend to create the Checkout Session
        fetch('/api/create-checkout-session', {
        method: 'POST',
        })
        .then(function(response) {
        return response.json();
        })
        .then(function(session) {
        return stripe.redirectToCheckout({ sessionId: session.id });
        })
        .then(function(result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, you should display the localized error message to your
        // customer using `error.message`.
        if (result.error) {
            alert(result.error.message);
        }
        });

    });*/

    /* création de la route secret qui retourne intent du paiment et execute le tranfere de font
    var response = fetch('/secret').then(function(response) {
        return response.json();
        }).then(function(responseJson) {
        var clientSecret = responseJson.client_secret;
        // Call stripe.confirmCardPayment() with the client secret.
        });
        */
});
</script>
@endsection
