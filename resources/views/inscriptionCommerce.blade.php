@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Inscription Commerce</h2>
            </div>

            @if($shop==null)
            <!--Formulaire -->
            <div class="justify-content-xl-center align-items-center" style="padding: 0% 20%">
                <form  enctype="multipart/form-data" method="post" name="inscriptionCommerceForm" action="{{route('inscriptionCommerceForm')}}">
                    @csrf
                    <h3 style="display: flex; align-items: center; justify-content: center;"> Informations générales </h3>
                    <br>
                    <div class="form-group">
                        <label>Nom du commerce: </label>
                        <input name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Type de commerce: </label>
                        <select class="form-control" name="typeShop_id">
                            @foreach($typeShops as $typeShop)
                                <option value="{{$typeShop->id}}">{{$typeShop->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Siren: </label>
                        <input name="siren" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Siret: </label>
                        <input name="siret" class="form-control" required>
                    </div>


                    <div class="form-group">
                        <input type="hidden" name="entity_id" class="form-control" value=1>
                        <!-- TODO attention changer la value en la valeur de l'entity du commerce genre entity_id -->
                    </div>
                    <div class="form-group">
                        <label>Adresse: </label>
                        <input name="address" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Code postal: </label>
                        <input name="zip_code" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Ville: </label>
                        <input name="city" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Numéro de téléphone : </label>
                        <input name="numero" class="form-control" required>
                    </div>

                    <h3 style="display: flex; align-items: center; justify-content: center;" > Mode d'achat autorisé </h3>
                    <br>
                    <div class="form-group">
                        <label>Click and Collect </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect1"
                                   value="true">
                            <label class="form-check-label" for="clickAndCollect1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect2"
                                   value="false">
                            <label class="form-check-label" for="clickAndCollect2">Non</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Livraison à domicile : </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome1"
                                   value="true">
                            <label class="form-check-label" for="deliveryHome1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome2"
                                   value="false">
                            <label class="form-check-label" for="deliveryHome2">Non</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Livraison en point relay : </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint1"
                                   value="true">
                            <label class="form-check-label" for="deliveryPoint1">oui</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint2"
                                   value="false">
                            <label class="form-check-label" for="deliveryPoint2">Non</label>
                        </div>
                    </div>


                    <h3 style="display: flex; align-items: center; justify-content: center;"> Documents annexes</h3>
                    <br>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" id="image" accept="image/png" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Preuve de bail en format PDF</label>
                        <input type="file" name="document" accept=".pdf" class="form-control" required>
                    </div>

                    <!--on met le state en attente : Waiting-->
                    <div class="form-group">
                        <input type="hidden" name="state" class="form-control" value="Waiting">
                    </div>

                    <div class="d-grid col-3 mx-auto">
                        <button type="submit" class='btn btn-success btn-lg'> Inscrire mon commerce</button>
                    </div>

                </form>

            </div>
            <br><br><br><br>

            <!-- TODO après association du compte avec le shop dans la création d'un shop récupérer le commentaire du coup et pas refaire un créate mais un update
            if entity_id est associé a une ligne shop
             récup le commentaire
             update le formulaire-->

            @else
                <h3 class="text-center" style="color:red;"> Voici le commentaire de la mairie : {{ $shop->comment }}</h3>
                <div class="justify-content-xl-center align-items-center" style="padding: 0% 20%">
                    <form  enctype="multipart/form-data" method="post" name="inscriptionCommerceForm" action="{{route('inscriptionCommerceForm')}}">
                        @csrf
                        <h3 style="display: flex; align-items: center; justify-content: center;"> Informations générales </h3>
                        <br>
                        <div class="form-group">
                            <label>Nom du commerce: </label>
                            <input name="name" class="form-control" value="{{$shop->name}}" required>
                        </div>
                        <div class="form-group">
                            <label>Type de commerce: </label>
                            <select class="form-control" name="typeShop_id">
                                <option value="{{$type->id}}">{{$type->name}}</option>
                                @foreach($typeShops as $typeShop)
                                    <option value="{{$typeShop->id}}">{{$typeShop->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Siren: </label>
                            <input name="siren" class="form-control" value="{{$shop->siren}}" required>
                        </div>
                        <div class="form-group">
                            <label>Siret: </label>
                            <input name="siret" class="form-control" value="{{$shop->siret}}" required>
                        </div>


                        <div class="form-group">
                            <input type="hidden" name="entity_id" class="form-control" value=1>
                            <!-- TODO attention changer la value en la valeur de l'entity du commerce genre entity_id -->
                        </div>
                        <div class="form-group">
                            <label>Adresse: </label>
                            <input name="address" class="form-control" value="{{$entity->address}}" required>
                        </div>
                        <div class="form-group">
                            <label>Code postal: </label>
                            <input name="zip_code" class="form-control" value="{{$entity->zipCode}}" required>
                        </div>
                        <div class="form-group">
                            <label>Ville: </label>
                            <input name="city" class="form-control" value="{{$entity->city}}" required>
                        </div>
                        <div class="form-group">
                            <label>Numéro de téléphone : </label>
                            <input name="numero" class="form-control" value="{{$entity->numero}}" required>
                        </div>

                        <h3 style="display: flex; align-items: center; justify-content: center;" > Mode d'achat autorisé </h3>
                        <br>
                        <div class="form-group">
                            <label>Click and Collect </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect1"
                                       value="true">
                                <label class="form-check-label" for="clickAndCollect1">oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="clickandcollect" id="clickAndCollect2"
                                       value="false">
                                <label class="form-check-label" for="clickAndCollect2">Non</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Livraison à domicile : </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome1"
                                       value="true">
                                <label class="form-check-label" for="deliveryHome1">oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="deliveryHome" id="deliveryHome2"
                                       value="false">
                                <label class="form-check-label" for="deliveryHome2">Non</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Livraison en point relay : </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint1"
                                       value="true">
                                <label class="form-check-label" for="deliveryPoint1">oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="deliveryPoint" id="deliveryPoint2"
                                       value="false">
                                <label class="form-check-label" for="deliveryPoint2">Non</label>
                            </div>
                        </div>


                        <h3 style="display: flex; align-items: center; justify-content: center;"> Documents annexes</h3>
                        <br>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" name="image" id="image" accept="image/png" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Preuve de bail en format PDF</label>
                            <input type="file" name="document" accept=".pdf" class="form-control" required>
                        </div>

                        <!--on met le state en attente : Waiting-->
                        <div class="form-group">
                            <input type="hidden" name="state" class="form-control" value="Waiting">
                        </div>

                        <div class="d-grid col-3 mx-auto">
                            <button type="submit" class='btn btn-success btn-lg'> Inscrire mon commerce</button>
                        </div>

                    </form>

                </div>
            @endif


        </div>


        <style type="text/css">
            .container {
                width: 900px;
            }
        </style>



@endsection
