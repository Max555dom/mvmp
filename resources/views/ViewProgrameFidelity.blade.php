@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->

<div class='row'>
    <div class="container-fluid programme">

        <!-- Programme -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary text-center">Visualisation de votre programme de fidélité</h2>
            </div>
            <div class="card-body">  
              <form id='add-form'>
                <div class="form-row">
                  <div class="form-group">
                        <img src="{{ asset('storage/ImagesShopProgram/'.$programme[0]->image) }}"  id="image" class='img-fluid' alt=''>
                  </div>
                </div>
                <h3 class="text-center"><?php echo $programme[0]->name ?></h3>
                <br>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pointsByCommande">Points attribués par commande</label>
                    <h4 class="text-center"><?php echo $programme[0]->pointCommande ?><h4>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="minPriceByCommande">Prix minimum de la commande</label>
                    <h4 class="text-center"><?php echo $programme[0]->minPriceByCommande ?><h4>
                  </div>
                </div>

                <label for="description">Description</label>
                <p><?php echo $programme[0]->description ?></p>
                <br>
              </form>
                <div class="text-center">
                  <button type='button-edit' data-toggle="modal" data-target="#modal-programme-edit" class="btn btn-warning">Modifier</button>
                  <button type='button-supp' data-toggle="modal" data-target="#modal-programme-supp" class="btn btn-danger">Supprimer</button>
                </div>
              
              

              
            </div>
        </div>
    </div>
    <div class="container-fluid pallier">
      <!-- Palliers -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary text-center">Les palliers de votre programme de fidélité</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive text-center">
            <button type='button' class="btn btn-success" data-toggle="modal" data-target="#modal-pallier">Création d'un pallier</button>
            <hr>
              <?php 
                //dd($palliers);
                foreach($palliers as $OnePallier)
                {
              ?>
                  <div class="card shadow pallier-form">
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <br>
                        <h4><?php echo $OnePallier->name ?></h4>
                        
                      </div>
                      <div class="form-group col-md-4">
                        <br>
                        <br>
                        <label for="objectif">Objectif de point</label>
                        <h4><?php echo $OnePallier->objectifPoint ?></h4>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="bonus">Bonus</label>
                        <h4><?php echo $OnePallier->bonus ?></h4>
                      </div>

                      <div class="form-group col-md-4">
                        <br>
                        <button type='button' onClick="deletePallier('<?php echo $OnePallier->id;?>');" class="btn btn-danger">Supprimer</button> <!-- Ajout d'un on click avec l'id du programme en parametre pour le supprimer -->
                      </div>
                    </div>

                    <label for="description">Description</label>
                    <p><?php echo $OnePallier->description ?></p>
                        
                  </div>
               
              <?php
                    
                  }

              ?>  
          </div>  
        </div>
      </div>
    </div>
</div>


<!-- Modal Pallier-->
<div class="modal" id='modal-success-supp' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Suppression du pallier pour votre programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre supression à bien était effectué</p>
      </div>
      <div class="modal-footer">
      <form action='/programeFidelity/<?php echo $programme[0]->shop_id;?>' method="GET">
        <button type="submit" class="btn btn-success" >Fermer</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec-supp" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Echec de la suppression du pallier a votre programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Erreur dans de la suppression de votre pallier de programme de fidélité, veuillez vous assurez de la validité des champs saisits</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" id='modal-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Création d'un nouveau pallier pour votre programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Votre ajout à bien était effectué</p>
      </div>
      <div class="modal-footer">
      <form action='/programeFidelity/<?php echo $programme[0]->shop_id;?>' method="GET">
        <button type="submit" class="btn btn-success" >Fermer</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id ="modal-echec" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Echec de l'ajout pallier a votre programme de fidélité</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Erreur dans l'ajout de votre pallier de programme de fidélité, veuillez vous assurez de la validité des champs saisits</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-pallier' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Création d'un nouveau pallier pour le programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id='add-pallier-form' >
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name_pallier">Nom du pallier</label>
              <input name="name_pallier" id="name_pallier" type="text" class="form-control"  placeholder="pallier gold..." required/>
            </div>
           
          </div>
          

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="objectif">Objectif de point</label>
              <input name= "objectif" id="objectif" type="text" class="form-control"  placeholder="100" required/>
            </div>

            <div class="form-group col-md-6">
              <label for="bonus">Bonus</label>
              <input name= "bonus" id="bonus" type="text" class="form-control"  placeholder="-5%" required/>
            </div>
          </div>

          <label for="commentaire">Description</label>
          <div class="form-row">
            <div class="form-group ">
              <textarea id="commentaire" name="commentaire" rows="3" cols="60" maxlength="255" required></textarea>
            </div> 
              
          </div>
        </div>
      <div class="modal-footer">
        <button type='submit' class="btn btn-success">Valider</button> 
      </form>
        <button type="submit" data-dismiss="modal" class="btn btn-danger" >Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-pallier-delete' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Suppression d'un pallier du programme fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id='del-paller-form' >
          <p>Etes vous sure de vouloir supprimer ce pallier de votre programme de fidélité ?</p>
         
      </div>
      <div class="modal-footer">
        <button type='submit' class="btn btn-success">Valider</button> 
      </form>
        <button data-dismiss="modal" class="btn btn-danger" >Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Programme-->
<div class="modal" id='modal-programme-edit' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Modification du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id='edit-programme-form' >
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name_pallier_edit">Nom du programme</label>
              <input name="name_pallier_edit" id="name_pallier_edit" type="text" value="<?php echo $programme[0]->name ?>" class="form-control"  placeholder="pallier gold..." required/>
            </div>
          </div>
          <label for="description_edit">Description</label>
          <div class="form-row">
            <div class="form-group ">
              <textarea id="description_edit" name="description_edit" rows="3" cols="60"  maxlength="255" required><?php echo $programme[0]->description ?></textarea>
            </div> 
              
          </div>
        </div>
      <div class="modal-footer">
        <button type='submit' class="btn btn-success">Valider</button> 
      </form>
        <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-programme-edit-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Modification du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>Votre programme de fidélité a bien était validé !</p>
      </div>
      <div class="modal-footer">
      <form action='/programeFidelity/<?php echo $programme[0]->shop_id;?>' method="GET">
        <button type='submit' class="btn btn-success" >Fermer</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-programme-edit-fail' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Modification du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>Echec de la modification de votre programme de fidélité</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" id='modal-programme-supp' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Suppression du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Etes vous sure de vouloir supprimer votre programme de fidélité ?</p>
      </div>
      <div class="modal-footer">
        <form id='del-programme-form'>
          <button type='submit' class="btn btn-success">Valider</button>
          <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-programme-supp-success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Suppression du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Succée de la suppression du programme de fidélité !</p>
      </div>
      <div class="modal-footer">
        
      <form action='/programeFidelity/<?php echo $programme[0]->shop_id;?>' method="GET">
        <button type='submit' class="btn btn-success" >Fermer</button>
      </form>
     
      </div>
    </div>
  </div>
</div>

<div class="modal" id='modal-programme-supp-fail' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-center">Suppression du programme de fidélité</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Echec de la suppression du programme de fidélité, car celui-ci à moins de 60 jours de durée d'activité</p>
      </div>
      <div class="modal-footer">
        
        <button data-dismiss="modal" class="btn btn-danger" >Fermer</button>
     
      </div>
    </div>
  </div>
</div>


<style type="text/css">

.programme{
    width: 40%;
    margin-left: 5%;
}
.pallier{
    margin-right: 15%;
    width: 40%;

}
#add-form{
  margin-left: 0%;
}

label{
  color: black;
}
#image {
    margin-left: 25%;
    max-width: 50%;
    height: auto;
}

.pallier-form{
  max-width: 90%;
  margin-left: 5%;
}


</style>
<script>

var idProgramme = @json($programme[0]->id);
var date = @json($programme[0]->created_at);
var idPallier;
$(document).ready(function() {
    //Display the menu on side-bar
    $('#collapseNavigation').collapse();
    // Handle form submission.
    var form1 = document.getElementById('add-pallier-form');

    form1.addEventListener('submit', function(event) {
        event.preventDefault();
       
        var form_data1  = new FormData();
        
        var name_pallier = document.getElementById('name_pallier').value;
        form_data1.append("name_pallier",name_pallier);

        var objectif = document.getElementById('objectif').value;
        form_data1.append("objectif",objectif);

        var bonus = document.getElementById('bonus').value;
        form_data1.append("bonus",bonus);
        
        var commentaire = document.getElementById('commentaire').value;
        form_data1.append("commentaire",commentaire);

        form_data1.append("idProgramme",idProgramme);

        $.ajax({
          url: '/api/add/programme/pallier',
          type: 'POST',
          data: form_data1,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            $('#modal-pallier').modal('hide');
            $('#modal-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('#modal-pallier').modal('hide');
            $('#modal-echec').modal('show');
          }
        });
     
    });


    // Handle form submission.
    var form2 = document.getElementById('edit-programme-form');

    form2.addEventListener('submit', function(event) {
        event.preventDefault();
       
        var form_data2  = new FormData();
        
        var name_pallier_edit = document.getElementById('name_pallier_edit').value;
        form_data2.append("name_pallier_edit",name_pallier_edit);
        
        var description_edit = document.getElementById('description_edit').value;
        form_data2.append("description_edit",description_edit);
       
        form_data2.append("idProgramme",idProgramme);
       
        $.ajax({
          url: '/api/edit/programme',
          type: 'POST',
          data: form_data2,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            $('#modal-programme-edit').modal('hide');
            $('#modal-programme-edit-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $('#modal-programme-edit').modal('hide');
            $('#modal-programme-edit-fail').modal('show');
          }
        });
     
    });


    // Handle form submission.
    var form3 = document.getElementById('del-programme-form');

    form3.addEventListener('submit', function(event) {
        event.preventDefault();
       
        var form_data3  = new FormData();
        form_data3.append("idProgramme",idProgramme);
        form_data3.append("date",date);
      
        $.ajax({
          url: '/api/delete/programme',
          type: 'POST',
          data: form_data3,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            $('#modal-programme-supp').modal('hide');
            $('#modal-programme-supp-success').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {           
            $('#modal-programme-supp').modal('hide');
            $('#modal-programme-supp-fail').modal('show');
          }
        });
     
    });

    // Handle form submission.
    var form4 = document.getElementById('del-paller-form');

    form4.addEventListener('submit', function(event) {
        event.preventDefault();
       
        var form_data4  = new FormData();
        form_data4.append("idPallier",idPallier);

      
        $.ajax({
          url: '/api/delete/programme/pallier',
          type: 'POST',
          data: form_data4,
          dataType:'json',
          contentType: false,
          processData: false,
          success: function(data) {
            $('#modal-pallier-delete').modal('hide');
            $('#modal-success-supp').modal('show');
          },
          error: function(jqXHR, textStatus, errorThrown) {           
            $('#modal-pallier-delete').modal('hide');
            $('#modal-echec-supp').modal('show');
          }
        });
     
    });
    
/*
    $("#btn-upload").click(function(){
    var form_data  = new FormData();
    var file = document.getElementById('file').file;
    var name = file.name;
    form_data.append("file", file, name);
    
    $.ajax({
        url: '/api/image-upload',
        type: 'POST',
        data: form_data,
        dataType:'json',
        contentType: false,
        processData: false,
        success: function(data) {
         
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error on Upload Request');
        }
    });
  });
  */

});


function deletePallier(id_Pallier){
      console.log(id_Pallier);
      idPallier = id_Pallier;
     
      $('#modal-pallier-delete').modal('show');
      /*
      var form_data  = new FormData();

      form_data.append("pallier_id",idPallier);

      $.ajax({
        url: '/api/delete/pallier',
        type: 'POST',
        data: form_data,
        dataType:'json',
        contentType: false,
        processData: false,
        success: function(data) {
          //$('#modal-programme-delete').modal('hide');
          $('#modal-success-supp').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
          //$('#modal-programme-delete'').modal('hide');
          $('#modal-echec-supp').modal('show');
        }
      });
      */

    }
</script>
@endsection
