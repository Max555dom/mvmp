@extends('layouts.app')
@section('content')

  <!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary text-center">Les Commerces de ma ville</h2>
        </div>
        <div class="card-body">
     
            <div class="table-responsive text-center">
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis asperiores quibusdam, nam modi, consectetur saepe, neque amet deserunt iure quo voluptatibus sed! Iure cupiditate culpa distinctio inventore, vitae quae ab. Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis, et rerum nisi ab quidem inventore aliquid vitae blanditiis at mollitia ipsam </p>
                <br>  
  
            <div class="container">
                <hr>
                <div id="position" class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-success">
                                <input type="radio" name="pos" value="All" checked>All 
                            </label> 
                    <?php                  
                        foreach($resultsType ?? '' as $oneType)
                        {                  
                        ?>
                            <label class="btn btn-success">
                                <input type="radio" name="pos" value="<?php echo $oneType->id;?>"><?php echo $oneType->name;?>
                            </label>
                        <?php 
                        }
                        ?>
                </div>
                    <hr>
                   
                    <table id="example" class="display nowrap" width="100%"> 
                    </table>

            </div>

    
        </div>
    </div>
</div>
<style type="text/css">

/*Hide headers of dataTable*/
thead {
             position: absolute !important;
             top: -9999px !important;
             left: -9999px !important;
}

</style>
<script>

var columnDefs = @json($Columns);
// Call the dataTables jQuery plugin
$(document).ready(function() {
//Display the menu on side-bar
$('#collapseNavigation').collapse();
  
    //DataTable of shops 
    var table = $('#example').DataTable({
        columns: columnDefs,
        "scrollY":        "680px",
        "scrollCollapse": true,
        'bInfo': false,
        columnDefs: // for display an image
            [
                {
                    "targets": 1,
                    "data": '//placehold.it/200',
                    "render": function (data, type, row, meta) {
                        return   "<img src='" + row[1] + "' class='img-fluid' alt=''>";
                    }
                },
                {
                    "targets": 3,
                    "data": 'unknown',
                    "render": function (data, type, row, meta) {
                        return   "<td>" + row[2] +"<br><br>" + row[3] + "</td>";
                    }
                }
            ],
        'ajax': {
            url: '/api/commerces' ,
            method: 'GET'
        },
        'language' :  {
            'url': '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
        },
    });

     // Filter of dataTable
     $('input:radio').on('change', function () {
        //build a regex filter string with an or(|) condition
        var positions = $('input:radio[name="pos"]:checked').map(function() {
            return '^' + this.value + '$';
        }).get().join('|');
        if(positions == "^All$")
        {
            table.columns().search("").draw();
        }else{
            table.column(5).search(positions, true, false, false).draw(false);
        }
    });

    //Rediction of buton of each row
    $('#example tbody').on( 'click', 'label', function () {
            var data = table.row( $(this).parents('tr') ).data();
            window.location.href = "commerce/" + data[0];
    });

});
</script>
@endsection